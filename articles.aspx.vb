Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Partial Class articles
    Inherits System.Web.UI.Page
    Protected dr As SqlDataReader
    Protected ds As New DataSet
    Protected section As String
    Protected alertcolor As String
    Protected olddate As String
    Protected dateheader As String
    Protected dateheaderdg As String
    Protected picTag As String
    Protected tagPics As Integer = 0
    Protected tag As String



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.da = New System.Data.SqlClient.SqlDataAdapter

    End Sub
    Protected WithEvents DataList1 As System.Web.UI.WebControls.DataList
    Protected WithEvents btnSearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgArticles As System.Web.UI.WebControls.DataGrid
    Protected WithEvents da As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents ldgHeader As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub ShowArticles(ByVal searchstr As String)
        Dim dbNTB As New SqlConnection(ConfigurationManager.AppSettings("conString"))
        Dim cd As New SqlCommand
        Dim tjeneste, maingroup, maincat, subgroup, subcat, interval As String
        Dim numarticles As Integer = 0
        Dim pagecount As Integer = 0
        Dim articlecount As Integer = 0
        Dim archive As Integer = 0

        section = Request.QueryString("section")
        tjeneste = Request.QueryString("Tjeneste")
        maingroup = Request.QueryString("MainGroup")
        subgroup = Request.QueryString("SubGroup")
        maincat = Request.QueryString("MainCat")
        subcat = Request.QueryString("SubCat")

        interval = Request.QueryString("Interval")
        'articlecount = Request.QueryString("Count")
        If Request.QueryString("ArchiveCount") <> "" Then articlecount = CInt(Request.QueryString("ArchiveCount"))
        ' Trenger en avklaring p� hva som er mest hensiktmessig mtp antall saker/dager som skal vises, link til ark

        dbNTB.Open()
        cd.Connection = dbNTB
        'cd.CommandText = "select maingroup, subgroup, maincat, subcat, showsearch, articlelistheader, numarticles, pagecount, archive from sections where name=@section"
        cd.CommandText = "select tjeneste, maingroup, subgroup, maincat, subcat, showsearch, articlelistheader, numarticles, pagecount, archive, tagPicArticles from sections where name=@section"
        cd.Parameters.AddWithValue("@section", section)
        Try
            dr = cd.ExecuteReader()
            If dr.Read() Then
                If tjeneste = "" Then tjeneste = CStr(dr("Tjeneste"))
                If maingroup = "" Then maingroup = CStr(dr("MainGroup"))
                If subgroup = "" Then subgroup = CStr(dr("SubGroup"))
                If maincat = "" Then maincat = CStr(dr("MainCat"))
                If subcat = "" Then subcat = CStr(dr("SubCat"))

                pSearch.Visible = (dr("ShowSearch") = 1)
                lHeader.Text = CType(dr("ArticlelistHeader"), String)

                numarticles = dr("NumArticles")
                If articlecount > 0 Then numarticles = articlecount

                pagecount = dr("PageCount")
                archive = dr("Archive")
                tagPics = dr("TagPicArticles")
            End If
        Finally
            Server.ClearError()
            dr.Close()
        End Try

        cd.Parameters.Clear()
        cd.CommandType = CommandType.StoredProcedure
        If searchstr = "" Then
            cd.CommandText = "_GetArticles"

            If numarticles > 0 And interval = "" Then
                cd.Parameters.Add(New SqlParameter("@interval", SqlDbType.Int))
                cd.Parameters.Item("@interval").Value = numarticles * 24
            End If

            'If numarticles > 0 Then
            '    cd.Parameters.Add(New SqlParameter("@top", SqlDbType.Int))
            '    cd.Parameters.Item("@top").Value = numarticles
            'End If
        Else
            cd.CommandText = "_SearchArticles"
            cd.Parameters.Add(New SqlParameter("@searchstr", SqlDbType.VarChar, 100))
            cd.Parameters.Item("@searchstr").Value = searchstr
        End If

        If tjeneste <> "-1" Then
            cd.Parameters.Add(New SqlParameter("@filterTjeneste", SqlDbType.VarChar, 100))
            cd.Parameters.Item("@filterTjeneste").Value = tjeneste

        ElseIf System.Configuration.ConfigurationManager.AppSettings("HiddenTjeneste") <> "" Then
            cd.Parameters.Add(New SqlParameter("@hiddenTjeneste", SqlDbType.VarChar, 100))
            cd.Parameters.Item("@hiddenTjeneste").Value = System.Configuration.ConfigurationManager.AppSettings("HiddenTjeneste")
        End If

        If maingroup <> "" Then
            cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.VarChar, 255))
            cd.Parameters.Item("@maingroup").Value = BitCommaText(CLng(maingroup))
        End If

        If subgroup <> "" Then
            cd.Parameters.Add(New SqlParameter("@subgroup", SqlDbType.VarChar, 100))
            cd.Parameters.Item("@subgroup").Value = BitCommaText(CInt(subgroup))
        End If

        If maincat <> "" And maincat <> "-1" Then
            cd.Parameters.Add(New SqlParameter("@maincat", SqlDbType.VarChar, 255))
            cd.Parameters.Item("@maincat").Value = maincat
        End If

        If subcat <> "" And subcat <> "-1" Then
            cd.Parameters.Add(New SqlParameter("@subcat", SqlDbType.VarChar, 255))
            cd.Parameters.Item("@subcat").Value = subcat
        End If

        If interval <> "" Then
            If interval.IndexOf("-") > -1 Then
                cd.Parameters.Add(New SqlParameter("@interval", SqlDbType.Int))
                cd.Parameters.Item("@interval").Value = CInt(interval.Split("-")(0))

                cd.Parameters.Add(New SqlParameter("@intervalTo", SqlDbType.Int))
                cd.Parameters.Item("@intervalTo").Value = CInt(interval.Split("-")(1))
            Else
                cd.Parameters.Add(New SqlParameter("@interval", SqlDbType.Int))
                cd.Parameters.Item("@interval").Value = CInt(interval)
            End If
        End If

        Try

            If (section <> "IDAG" Or Session("UserID") <> "") Then
                dr = cd.ExecuteReader

                If Not dr.HasRows Then
                    hdr.Visible = True
                Else
                    hdr.Visible = False
                    dlArticles.DataSource = dr

                    dlArticles.DataBind()
                End If
            Else
                hdr.Visible = True
            End If

        Finally
            Server.ClearError()
            dr.Close()
        End Try

        'Log user hit
        If (Session("UserID") <> "") Then
            AddUserHit(Session("UserID"), False, False, Session("SiteCode"))
        End If

        ' Vis arkiv link
        If archive > 0 And CInt(Request.QueryString("ArchiveCount")) = 0 And Not dlArticles.ShowHeader And searchstr = "" Then
            hArchive.Visible = True
            hArchive.Text = ConfigurationManager.AppSettings("archivelinktext")
            hArchive.NavigateUrl = Request.RawUrl & "&ArchiveCount=" + CStr(archive)
        Else
            hArchive.Visible = False
        End If
        dbNTB.Close()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        tag = ConfigurationManager.AppSettings("PictureTag")

        Dim search As String = txtSearch.Text
        Dim fnutt As Boolean = False
        Dim prevFnutt As Boolean = False

        If search <> "" Then

            search = search.Replace("'", "").Trim()
            search = Regex.Replace(search, "\s{2,}", " ")
            'search = Regex.Replace(search, "\s""$", "")
            Dim i As Integer = 0
            While i < search.Length

                If search.Chars(i) = """" And Not fnutt Then
                    fnutt = True
                ElseIf search.Chars(i) = """" And fnutt Then
                    fnutt = False
                    prevFnutt = True
                ElseIf i = 0 Then
                    search = search.Insert(i, """")
                End If

                If search.Chars(i) = " " And Not fnutt Then
                    If Not prevFnutt Then
                        search = search.Insert(i, """ AND")
                        i += 6
                    Else
                        prevFnutt = False
                        search = search.Insert(i, " AND")
                        i += 5
                    End If

                    If search.Chars(i) = """" Then
                        i -= 1
                    Else
                        search = search.Insert(i, """")
                    End If

                End If

                i += 1
            End While

            If search.Chars(search.Length - 1) <> """" Then
                search &= """"
            End If

        End If

        ShowArticles(search)
    End Sub

    Private Sub dlArticles_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlArticles.ItemCreated
        Try
            If CType(sender, DataList).DataSource("Urgency") <= 3 Then alertcolor = "red" Else alertcolor = "black"
            If olddate <> Format(CType(sender, DataList).DataSource("CreationDatetime"), "yyyyMMdd") Then
                dateheader = "<br><b>" & Format(CType(sender, DataList).DataSource("CreationDatetime"), "dd. MMMM yyyy") & "</b><br>"
            Else
                dateheader = ""
            End If
            olddate = Format(CType(sender, DataList).DataSource("CreationDatetime"), "yyyyMMdd")

            If (CType(sender, DataList).DataSource("HasPictures") > 0 And tagPics > 0) Then
                picTag = tag
            Else
                picTag = ""
            End If

        Catch
        End Try
    End Sub

End Class
