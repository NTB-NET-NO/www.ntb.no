/*
 overlibmws_overtwo.js plug-in module - Copyright Foteos Macrides 2003-2004
   For support of the popups-within-a-popup feature.
   Initial: July 14, 2003 - Last Revised: March 22, 2004
 See the Change History and Command Reference for overlibmws via:

	http://www.macridesweb.com/oltest/

 License agreement for the standard overLIB applies.  Access license via:
	http://www.bosrup.com/web/overlib/license.html
*/

// PRE-INIT
OLloaded=false;

// INIT
var OLzIndex,over2=null,OLpuParent=null,OLpuParentDrop=null,OLpuParentDrag=0;
var OLpuParentRelx=null,OLpuParentRely=null,OLpuParentMidx=null,OLpuParentMidy=null;
var OLpuParentRef=null,OLpuParentRefc=null,OLpuParentRefp=null;
var OLpuParentRefx=null,OLpuParentRefy=null,OLpuParentScroll=0;
var OLpuParentIfShim=null,OLpuParentIfShimShadow=null;

/////////
// PUBLIC FUNCTIONS
/////////
function overlib2(){
var args=overlib2.arguments;
if(!o3_showingsticky||args.length==0||o3_mouseoff||o3_noclose)return;
if(OLdraggablePI&&o3_draggable){OLpuParentDrag=1;o3_draggable=0;}
if((OLshadowPI)&&bkdrop){OLpuParentDrop=bkdrop;bkdrop=null;}
if((OLiframePI)&&OLifShim){OLpuParentIfShim=OLifShim;OLifShim=null;
if(OLifShimShadow){OLpuParentIfShimShadow=OLifShimShadow;OLifShimShadow=null;}}
OLpuParentRelx=o3_relx;OLpuParentRely=o3_rely;OLpuParentMidx=o3_midx;OLpuParentMidy=o3_midy;
OLpuParentRef=o3_ref;OLpuParentRefc=o3_refc;OLpuParentRefp=o3_refp;OLpuParenRefx=o3_refx;
OLpuParenRefy=o3_refy;if(OLscrollPI){OLclearScroll();OLpuParentScroll=o3_scroll;o3_scroll=0;}
OLresetDefaults2();
OLparseTokens('o3_',args);
dispSecondPU();
}

function nd2(){
if(OLpuParentDrop){bkdrop=OLpuParentDrop;OLpuParentDrop=null;}
if(OLpuParentDrag){o3_draggable=1;OLpuParentDrag=0;}
if(over2)hideSecondObject(over2);
if(OLpuParent){over=OLpuParent;OLpuParent=null;}
if((OLiframePI)&&OLpuParentIfShim){OLifShim=OLpuParentIfShim;OLpuParentIfShim=null;
if(OLpuParentIfShimShadow){OLifShimShadow=OLpuParentIfShimShadow;OLpuParentIfShimShadow=null;}}
o3_relx=OLpuParentRelx;o3_rely=OLpuParentRely;o3_midx=OLpuParentMidx;o3_midy=OLpuParentMidy;
o3_ref=OLpuParentRef;o3_refc=OLpuParentRefc;o3_Refp=OLpuParentRefp;o3_refx=OLpuParentRefx;
o3_refy=OLpuParentRefy;
if(OLscrollPI){o3_scroll=OLpuParentScroll;OLpuParentScroll=0;if(o3_scroll){placeLayer();}}
if(OLhidePI)OLhideUtil(0,0,0);
}

/////////
// SUPPORT FUNCTIONS
/////////
function OLresetDefaults2(){
// Load defaults to runtime for secondary popups
o3_text=ol_text;
o3_cap=ol_cap;
o3_nofollow=ol_nofollow;
o3_background=ol_background;
o3_hpos=ol_hpos;
o3_offsetx=ol_offsetx;
o3_offsety=ol_offsety;
o3_fgcolor=ol_fgcolor;
o3_bgcolor=ol_bgcolor;
o3_cgcolor=ol_cgcolor;
o3_textcolor=ol_textcolor;
o3_capcolor=ol_capcolor;
o3_width=ol_width;
o3_wrap=ol_wrap;
o3_height=ol_height;
o3_border=ol_border;
o3_base=ol_base;
o3_snapx=ol_snapx;
o3_snapy=ol_snapy;
o3_fixx=ol_fixx;
o3_fixy=ol_fixy;
o3_relx=ol_relx;
o3_rely=ol_rely;
o3_midx=ol_midx;
o3_midy=ol_midy;
o3_ref=ol_ref;
o3_refc=ol_refc;
o3_refp=ol_refp;
o3_refx=ol_refx;
o3_refy=ol_refy;
o3_fgbackground=ol_fgbackground;
o3_bgbackground=ol_bgbackground;
o3_cgbackground=ol_cgbackground;
o3_padxl=ol_padxl;
o3_padxr=ol_padxr;
o3_padyt=ol_padyt;
o3_padyb=ol_padyb;
o3_fullhtml=ol_fullhtml;
o3_vpos=ol_vpos;
o3_capicon=ol_capicon;
o3_textfont=ol_textfont;
o3_captionfont=ol_captionfont;
o3_textsize=ol_textsize;
o3_captionsize=ol_captionsize;
if(OLfunctionPI)o3_function=ol_function;
o3_hauto=ol_hauto;
o3_vauto=ol_vauto;
o3_nojustx=ol_nojustx;
o3_nojusty=ol_nojusty;
o3_fgclass=ol_fgclass;
o3_bgclass=ol_bgclass;
o3_cgclass=ol_cgclass;
o3_textpadding=ol_textpadding;
o3_textfontclass=ol_textfontclass;
o3_captionpadding=ol_captionpadding;
o3_captionfontclass=ol_captionfontclass;
OLpuParent=null;
}

function dispSecondPU(){
var layerHtml;
over2=(OLns4?o3_frame.document.layers['overDiv2']:(OLie4?o3_frame.document.all['overDiv2']:
(OLns6?o3_frame.document.getElementById('overDiv2'):null)));
if(typeof over2=='undefined'||!over2){
if(OLns4&&document.classes){over2=o3_frame.document.layers['overDiv2']=new Layer(1024,o3_frame);
}else if(OLie4||OLns6){var body=(OLie4?o3_frame.document.all.tags('body')[0]:
o3_frame.document.getElementsByTagName('body')[0]);
over2=o3_frame.document.createElement('div');over2.id='overDiv2';
with(over2.style){position='absolute';visibility='hidden';}
body.appendChild(over2);}}
if(typeof over2=='undefined'||!over2)return;
OLpuParent=over;
over=over2;
if(o3_fgbackground!='')o3_fgbackground=' background="'+o3_fgbackground+'"';
if(o3_bgbackground!='')o3_bgbackground=' background="'+o3_bgbackground+'"';
if(o3_cgbackground!='')o3_cgbackground=' background="'+o3_cgbackground+'"';
if(o3_fgcolor!='')o3_fgcolor=' bgcolor="'+o3_fgcolor+'"';
if(o3_bgcolor!='')o3_bgcolor=' bgcolor="'+o3_bgcolor+'"';
if(o3_cgcolor!='')o3_cgcolor=' bgcolor="'+o3_cgcolor+'"';
if(o3_background==''&&!o3_fullhtml){
if(o3_height>0)o3_height=' height="'+o3_height+'"';
else o3_height='';}
if(typeof OLzIndex=='undefined')OLzIndex=getDIVzIndex();
if (OLie4||OLns6){
over2.style.backgroundImage=(o3_background?'url('+o3_background+')':'none');
over2.style.zIndex=OLzIndex+2;
}else if(OLns4){
over2.background.src=(o3_background?o3_background:null);
over2.zIndex=OLzIndex+2;}
layerHtml=(o3_background!=""||o3_fullhtml)?
ol_content_background(o3_text,o3_background,o3_fullhtml):
(o3_cap)?ol_content_caption(o3_text,o3_cap,""):ol_content_simple(o3_text);
if(o3_wrap&&!o3_fullhtml){
if(OLie4)repositionTo(over,
eval('o3_frame.'+docRoot+'.scrollLeft'),eval('o3_frame.'+docRoot+'.scrollTop'));
else if(OLns6)repositionTo(over,(o3_frame.pageXOffset+20),o3_frame.pageYOffset);
writeSecondLayer(layerHtml,over2);
o3_width=(OLns4?over.clip.width:over.offsetWidth);
o3_wrap=0;
layerHtml=((o3_background!=""||o3_fullhtml)?
ol_content_background(o3_text,o3_background,o3_fullhtml):
(o3_cap)?ol_content_caption(o3_text,o3_cap,''):ol_content_simple(o3_text));}
writeSecondLayer(layerHtml,over2);
if(o3_ref)refPosition=getRefLocation(o3_ref);
if(o3_ref&&refPosition[0]==null){o3_ref='';o3_midx=0;o3_midy=0;}
if(OLiframePI)initIframeRef();
if(OLiframePI)dispIfShim();
placeLayer();
o3_showid=setTimeout("showSecondObject(over2)",1);
o3_allowmove=(o3_nofollow?0:1);
}

function getDIVzIndex(id){
var obj;
if(id==''||id==null)id='overDiv';
obj=(OLns4?o3_frame.document.layers[id]:(document.all?o3_frame.document.all[id]:
o3_frame.document.getElementById(id)));
obj=(OLns4?obj:obj.style);
return obj.zIndex;
}

function writeSecondLayer(theHtml,obj){
theHtml += '\n';
if(OLns4){
var lyr=obj.document
lyr.write(theHtml)
lyr.close()
}else if(typeof obj.innerHTML != 'undefined'){
if(OLie5&&OLieM)obj.innerHTML=''
obj.innerHTML=theHtml}
}

function showSecondObject(obj){
o3_showid=0;
obj=(OLns4?obj:obj.style);
obj.visibility='visible';
if((OLiframePI)&&OLifShimOvertwo)OLifShimOvertwo.style.visibility="visible";
if(OLhidePI)OLhideUtil(1,1,0);
}

function hideSecondObject(obj){
if(o3_showid>0){clearTimeout(o3_showid);o3_showid=0;}
if((OLiframePI)&&OLifShimOvertwo)OLifShimOvertwo.style.visibility="hidden";
obj=(OLns4?obj:obj.style);
obj.visibility='hidden';
o3_allowmove=o3_nofollow=0;
}

OLovertwoPI=1;
OLloaded=1;
