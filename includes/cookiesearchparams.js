//	Copyright (C) 2000,2001 WebPlan as

	var csp_f;
	var csp_cname;
	var csp_cwork;
		
	function csp_MakeSelectCookie(fs) {
		cval = "";
		for (j=0;j<fs.length;j++) {
			if (fs[j].selected) {
				cval += "|" + fs[j].value;
			}
		}
		if (cval.length > 0) { return cval.substr(1); }
		else { return "" }
	}

	function csp_GetCheckedRadioIndex(rn) {
		fr = eval("csp_f."+rn);
		idx = -1;
		for (j=0;j<fr.length;j++) {
			if (fr[j].checked) {
				idx = j;
			}	
		}
		return idx;
	}
	
	function pbsSetCookie(excludelist) {
		var ca = new Array;
		var xl = "";
		if ((excludelist != null) && (!excludelist.name)) { xl = ","+excludelist+","; }
		for (var i=0;i<csp_f.length;i++) {
			if ((xl == "") || (xl.indexOf(","+csp_f[i].name+",") != -1)) {
				if (csp_f[i].type == "text") { 
					ca.length++;
					ca[ca.length-1] = csp_f[i].name + "=" + escape(csp_f[i].value);
				} else if ((csp_f[i].type == "select-multiple") || (csp_f[i].type == "select-one")) { 
					ca.length++;
					ca[ca.length-1] = csp_f[i].name + "=" + escape(csp_MakeSelectCookie(csp_f[i]));
				} else if (csp_f[i].type == "checkbox") {
					ca.length++;
					ca[ca.length-1] = csp_f[i].name + "=" + (csp_f[i].checked ? "1" : "0");
				} else if (csp_f[i].type == "radio") {
					if (csp_f[i].checked) {
						ca.length++;
						ca[ca.length-1] = csp_f[i].name + "=" + csp_GetCheckedRadioIndex(csp_f[i].name);
					}
				}
			}
		}
		if (ca.length > 0) {
			var cstr = ca.join("&") + ';';
			edate = new Date;
			edate.setFullYear(edate.getFullYear()+1);
			a = edate.toUTCString().split(" ");
			document.cookie = csp_cname + "=" + cstr + "Expires="+a[0]+" "+a[1]+"-"+a[2]+"-"+a[3].substr(2)+" "+a[4]+" GMT;Path=/";
		}
	}

	function csp_GetCookieVal(cn) {
		val = csp_cwork.substr(csp_cwork.indexOf(cn+'='));
		val = val.substr(val.indexOf('=')+1);
		val = val.substr(0,val.indexOf('&'));
		return val;
	}
	
	function csp_CookieSetSelect(fs, cn) {
		if (document.cookie.indexOf(cn+"=") > -1) {
			val = "|"+unescape(csp_GetCookieVal(cn))+"|";
			for (k=0;k<fs.length;k++) { 
				fs[k].selected = val.indexOf("|"+fs[k].value+"|") > -1;
			}
		}
	}

    function csp_CookieSetText(fi, cn) {
		if (document.cookie.indexOf(cn+"=") > -1) {
			fi.value = unescape(csp_GetCookieVal(cn));
		}
	}

	function csp_CookieSetCheckbox(fc, cn) {
		if (document.cookie.indexOf(cn+"=") > -1) {
			fc.checked = csp_GetCookieVal(cn) == "1";
		}
	}
    
	function csp_CookieSetRadio(cn) {
		if (document.cookie.indexOf(cn+"=") > -1) {
			idx = csp_GetCookieVal(cn);
			if ((idx>-1) && (eval("csp_f."+cn)[idx])) {
				eval("csp_f."+cn)[idx].checked = true;
			}
		}
	}
    
    function InitCookieSearchParams(f) {
		csp_f = f;
		if (csp_f.cookiename) {
			csp_cname = csp_f.cookiename.value;
		} else {
			csp_cname = f.name;
		}
		if (document.cookie.indexOf(csp_cname+"=") != -1) {
			csp_cwork = document.cookie.substr(document.cookie.indexOf(csp_cname+"=")+csp_cname.length+1);
			if (csp_cwork.indexOf(";") != -1) { csp_cwork = csp_cwork.substr(0,csp_cwork.indexOf(";")) }
			csp_cwork += "&";
			for (i=0;i<csp_f.length;i++) {
				if (csp_f[i].type == "text") { csp_CookieSetText(csp_f[i], csp_f[i].name); }
				else if ((csp_f[i].type == "select-multiple") || ((csp_f[i].type == "select-one"))) { csp_CookieSetSelect(csp_f[i], csp_f[i].name); }
				else if (csp_f[i].type == "checkbox") {	csp_CookieSetCheckbox(csp_f[i], csp_f[i].name); }
				else if (csp_f[i].type == "radio") { csp_CookieSetRadio(csp_f[i].name); }
			}
		}
	}

    function InitCookieSearchParamsOnly(f) {
		csp_f = f;
		if (csp_f.cookiename) {
			csp_cname = csp_f.cookiename.value;
		} else {
			csp_cname = f.name;
		}
	}

	if (document.searchform) { 
		InitCookieSearchParams(document.searchform)
	}
	
