<%@ Control Language="vb" AutoEventWireup="false" Codebehind="printer-friendly_list.ascx.vb" Inherits="ntb.printerfriendly_list" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<!-- Lists sections in a printer friendly manner -->

<asp:datalist CSSClass="artikkel_tekst" id="dlItems" runat="server" CellPadding="0" CellSpacing="0"
	RepeatLayout="Table">
	<ItemTemplate>
			<!-- <img src="<%# dr("TitleGif") %>"> -->
			<p class="artikkel_header"><%# titleMap(dr("name")) %></p>
			
			<p class="artikkel_tekst"><b><%# dr("ingress") %></b></p>
			<p class="artikkel_tekst"><%# dr("text") %></p>

			<p class="artikkel_bottomtekst"><%# dr("footer") %></p>

			<hr size=2 noshade>
			<br>
	</ItemTemplate>
</asp:datalist>
