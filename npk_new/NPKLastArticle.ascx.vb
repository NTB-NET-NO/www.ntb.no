﻿Imports System.Data.SqlClient
Imports System.Xml


Public Class NPKLastArticle
    Inherits System.Web.UI.UserControl

    Private _articleType As String = String.Empty
    Private _baseURL As String = "http://www.ntb.no"

    Public Property ArticleType() As String
        Get
            Return _articleType
        End Get
        Set(ByVal value As String)
            _articleType = value
        End Set
    End Property
    Public Property BaseURL() As String
        Get
            Return _baseURL
        End Get
        Set(ByVal value As String)
            _baseURL = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dbNTB As New SqlConnection(ConfigurationManager.AppSettings("conString"))
        Dim dr As SqlDataReader
        Dim cd As SqlCommand

        If (ArticleType = "INTERVJU") Then
            cd = New SqlCommand("SELECT top 1 RefId,CreationDateTime,MainGroup,articleTitle,ArticleXML,haspictures FROM ARTICLES " + _
                "WHERE (ntbFolderID = 32768) AND (HasPictures >= 1) AND (Subgroup = 1024) AND (maingroup & 23) > 0 ORDER BY CreationDateTime DESC")
        ElseIf (ArticleType = "FEATURE") Then
            cd = New SqlCommand("SELECT top 1 RefId,CreationDateTime,MainGroup,articleTitle,ArticleXML,haspictures FROM ARTICLES " + _
                "WHERE (ntbFolderID = 32768) AND (HasPictures >= 1) AND (Subgroup = 8192) AND (maingroup & 23) > 0 ORDER BY CreationDateTime DESC")
        Else 'Aktuelt
            cd = New SqlCommand("SELECT top 1 RefId,CreationDateTime,MainGroup,articleTitle,ArticleXML,haspictures FROM ARTICLES " + _
                "WHERE (ntbFolderID = 32768) AND (HasPictures >= 1) AND (Subgroup = 128) AND (maingroup & 23) > 0 ORDER BY CreationDateTime DESC")
            'Subgroup:      64 = Analyse : 128 = Bakgrunn
        End If


        dbNTB.Open()
        cd.Connection = dbNTB
        Try
            dr = cd.ExecuteReader()

            If dr.Read() Then
                dato.Text = CDate(dr("CreationDateTime")).ToString("[dd.MM.yyyy]")

                link.Text = CStr(dr("ArticleTitle"))
                link.Target = "npk_main"
                link.NavigateUrl = _baseURL + "article.aspx?Section=NPK_NYHENDE&RefID=" + CStr(dr("RefId")) + "&MainGroup=" + CStr(dr("MainGroup"))

                'Load up XML
                Dim doc As XmlDocument = New XmlDocument()
                doc.LoadXml(CStr(dr("ArticleXML")))

                Dim picid As String = doc.SelectSingleNode("//media[position() = 1]/media-reference/@source").InnerText
                bilde.ImageUrl = "http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_" + picid
            End If

        Finally
            'dr.Close()
        End Try



    End Sub

End Class