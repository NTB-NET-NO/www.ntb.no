﻿Public Class npk_direkte
    Inherits System.Web.UI.Page

    Protected baseURL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        baseURL = ConfigurationManager.AppSettings("NPKBaseURL")

        npk_last1.BaseURL = baseURL
        npk_last2.BaseURL = baseURL
        npk_last3.BaseURL = baseURL

    End Sub

End Class