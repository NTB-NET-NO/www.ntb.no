<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="ntb.npk_default" codePage="1252" %>
<%@ Register TagPrefix="custom" TagName="npk_last" src="NPKLastArticle.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="content-type" content="text/html; charset=Windows-1252">
    <title>Nynorsk Pressekontor - Heim</title>

    <link rel="stylesheet" type="text/css" href="npk_content/core.css">
    <link rel="stylesheet" type="text/css" href="npk_content/debug.css">
    <link rel="stylesheet" type="text/css" href="npk_content/npk.css">


<!-- Meta information START -->
<meta name="description" content="Heim ">
<meta name="MSSmartTagsPreventParsing" content="TRUE">

        <meta name="description" content="Pressbyr�et Nynorsk Pressekontor (NPK) leverer
redaksjonelle tenester til norske aviser og andre medium
p� nynorsk. Byr�et leverer mellom anna allment
nyhendestoff, foto, journalistiske oppdrag, nynorsk kryssord,
m.m." lang="no-nynorsk">
        <meta name="keywords" content="Nynorsk
Pressekontor, NPK, Norsk Telegrambyr�, NTB, nynorsk, presse, byr�
pressebyr�, telegrambyr�, aviser, avis, lokalavis,
blad, lokalblad, bygdeblad, journalistikk, media, spr�k,
dialekt, distrikt, m�lsak" lang="no-nynorsk">
        <meta name="distribution" content="Global">
        <meta name="copyright" content="Copyright � Nynorsk
Pressekontor (NPK)">
        <meta name="author" content="Nynorsk Pressekontor">
        <meta name="robots" content="Nofollow">
        <meta name="rating" content="General">
        <meta name="teststamp" content="1">
        
        <meta http-equiv="refresh" content="600">

<!-- Meta information END -->

</head>

<body leftmargin="0" topmargin="15" marginheight="15" marginwidth="0" text="#394F53" bgcolor="#CCCCCC">
<div align="center">
	<table width="840" bgcolor="#4E6D72" border="0" cellpadding="5" cellspacing="0">
		<tbody><tr>
			<td align="center">				<table width="830" border="0" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td colspan="9"><a href="http://www.npk.no"><img src="npk_content/logo.png" height="100" width="830" border="0"></a></td>
					</tr>
					<tr>
						<td><img src="npk_content/meny_1.gif" height="25" width="175" border="0"></td>
						<td><img src="npk_content/meny_sitemap.gif" height="25" width="65" border="0"></td>
						<td><img src="npk_content/meny_2.gif" height="25" width="590" border="0"></td>
					</tr>
				</tbody></table>				<table width="830" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0">
					<tbody><tr valign="top"> 
						<td width="160" bgcolor="#E5E5CC">
 
<div>

<img src="npk_content/innhald.png" alt="innhald" height="35" width="160" border="0">

</div>
<table width="160" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr> 
		<td class="leftmenuitem" width="17"><img src="npk_content/spacer.gif" height="1" width="17" border="0"></td>
		<td class="leftmenuitem" width="143"><img src="npk_content/spacer.gif" height="1" width="143" border="0"></td>
	</tr>
																												<tr> 
					<td class="leftmenuitem"><img src="npk_content/meny_arrow.gif" height="12" width="17" border="0"></td>
					<td class="leftmenuitem">
													<a href="<%= baseURL %>sectionwart.aspx?Section=NPK_NYHENDE" target="npk_main">nyhende</a>
											</td>
				</tr>
				
				<tr> 
					<td class="leftmenuitem"><img src="npk_content/meny_arrow.gif" height="12" width="17" border="0"></td>
					<td class="leftmenuitem">
													<a href="<%= baseURL %>sectionwart.aspx?Section=NPK_AKTUELT" target="npk_main">aktuelt</a>
											</td>
				</tr>
				
																														<tr> 
					<td class="leftmenuitem"><img src="npk_content/meny_arrow.gif" height="12" width="17" border="0"></td>
					<td class="leftmenuitem">
													<a href="<%= baseURL %>sectionwart.aspx?Section=NPK_FEATURE" target="npk_main">feature</a>
											</td>
				</tr>
				
																														<tr> 
					<td class="leftmenuitem"><img src="npk_content/meny_arrow.gif" height="12" width="17" border="0"></td>
					<td class="leftmenuitem">
													<a href="<%= baseURL %>sectionwart.aspx?Section=NPK_INTERVJU" target="npk_main">intervjuet</a>
											</td>
				</tr>
				
																														<tr> 
					<td class="leftmenuitem"><img src="npk_content/meny_arrow.gif" height="12" width="17" border="0"></td>
					<td class="leftmenuitem">
													<a href="<%= baseURL %>sectionwart.aspx?Section=NPK_DIREKTE" target="npk_main">npk direkte</a>
											</td>
				</tr>
				
																														<tr> 
					<td class="leftmenuitem"><img src="npk_content/meny_arrow.gif" height="12" width="17" border="0"></td>
					<td class="leftmenuitem">
													<a href="<%= baseURL %>sectionwart.aspx?Section=NPK_TEMA" target="npk_main">tema</a>
											</td>
				</tr>
				
																													
	</tbody></table>



<div class="vignetspacer">&nbsp;</div>

<div><a class="menyvignett" href="<%= baseURL %>sectionwart.aspx?Section=NPK_AKTUELT" target="npk_main" title="aktuelt nyheitsstoff">
<img border="0" src="npk_content/aktuelt.png" alt="aktuelt" width="63" height="15" />
</a></div>

<custom:npk_last id="npk_last1" ArticleType="AKTUELT" runat="server"></custom:npk_last> 
<div class="vignetspacer">&nbsp;</div>



<div><a class="menyvignett" href="<%= baseURL %>sectionwart.aspx?Section=NPK_FEATURE" target="npk_main" title="featuresaker">
<img border="0" src="npk_content/feature.png" alt="feature" width="68" height="15" />
</a></div>

<custom:npk_last id="npk_last2" ArticleType="FEATURE" runat="server"></custom:npk_last> 
<div class="vignetspacer">&nbsp;</div>



<div><a class="menyvignett" href="<%= baseURL %>sectionwart.aspx?Section=NPK_INTERVJU" target="npk_main" title="temaintervju">
<img border="0" src="npk_content/intervjuet.png" alt="intervjuet" width="78" height="15" />
</a></div>

<custom:npk_last id="npk_last3" ArticleType="INTERVJU" runat="server"></custom:npk_last> 

					
</div>


</td>
						<td width="10">&nbsp;</td>

						<td style="padding-top: 10px;" width="650">
							
 
<iframe src="<%= baseURL %>sectionwart.aspx?Section=NPK_DIREKTE" frameborder="0" width="100%" height="650" name="npk_main" ></iframe>



						</td>

					</tr>
					<tr>
						<td bgcolor="#E5E5CC"><img src="npk_content/spacer.gif" height="1" width="160"></td>
						<!--td colspan="4"><img src="npk_content/kolofon.gif" usemap="#Map2" height="90" width="430" border="0"></td-->
						<!--td><img src="npk_content/redaktoransvar.gif" usemap="#Map" height="90" width="140" border="0"></td-->
					</tr>				</tbody></table>
			</td>
		</tr>
	</tbody></table>
</div>

<map name="Map2">
    <area shape="rect" coords="136,76,212,85" href="mailto:kirsten@npk.no">

	<area shape="rect" coords="161,61,218,72" href="mailto:npk@npk.no">
	
</map>

</body></html>

