<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tipping.aspx.vb" Inherits="ntb.tipping"%>
<%@ Outputcache Duration="30" Location="Any" VaryByParam="*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tipping</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<table cellSpacing="0" cellPadding="0" width="644" border="0" class="artikkel_tekst">
			<tr>
				<td vAlign="top"><font face="Verdana"> <img src="images/t.gif" height="4" width="1"><br>
						<table width="100%">
							<tr>
								<td width="30"></td>
								<td align="center"><font face="Verdana" color="#666666" size="1">
										<h3><b>De siste spillresultater fra</b><br>
											<IMG src="../images/tipping/norsktip.jpg"></h3>
									</font>
								</td>
							</tr>
						</table>
						<table cellSpacing="0" cellPadding="2" width="100%" border="1" class="artikkel_tekst">
							<tr>
								<td align="center"><br>
									<IMG src="../images/tipping/tipping.jpg"><br>
									Midtuke<br>
									<table>
										<tr>
											<td><asp:datalist id="dlTippingM" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
								<td align="center"><br>
									<IMG src="../images/tipping/tipping.jpg"><br>
									L�rdag<br>
									<table>
										<tr>
											<td><asp:datalist id="dlTippingL" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
								<td align="center"><br>
									<IMG src="../images/tipping/tipping.jpg"><br>
									S�ndag<br>
									<table>
										<tr>
											<td><asp:datalist id="dlTippingS" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<table>
										<tr>
											<td align="center"><br>
												<IMG src="../images/tipping/oddsen.jpg"><br>
											</td>
											<td align="center" width="220">&nbsp;</td>
											<td align="center"><br>
												<IMG src="../images/tipping/Keno.jpg"><br>
											</td>
										</tr>
										<tr>
											<td><asp:datalist id="dlOddsen" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
											<td>&nbsp;</td>
											<td valign =top><asp:datalist id="dlKeno" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center"><br>
									<IMG src="../images/tipping/vlotto.jpg"><br>
									<table>
										<tr>
											<td><asp:datalist id="dlVLotto" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
								<td align="center"><br>
									<IMG src="../images/tipping/lotto.jpg"><br>
									<table>
										<tr>
											<td><asp:datalist id="dlLotto" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
								<td align="center"><br>
									<IMG src="../images/tipping/joker.jpg"><br>
									<table>
										<tr>
											<td><asp:datalist id="dlJoker" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center" colSpan="3"><br>
									<IMG src="../images/tipping/extra.jpg"><br>
									<table>
										<tr>
											<td><asp:datalist id="dlExtra" runat="server">
													<ItemTemplate>
														<P><A href="bilder/<%# GetFileName(Container.DataItem) %>"><font face="Verdana" color="#666666" size="1"><%# GetFileText(Container.DataItem) %></font></A>
															- <font face="Verdana" color="#666666" size="1">
																<%# GetFileDate(Container.DataItem, "dd.MM.yy - HH:mm")%>
															</font>
														</P>
													</ItemTemplate>
												</asp:datalist></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</font>
				</td>
			</tr>
		</table>
	</body>
</HTML>
