Imports System.IO

Partial Class tipping
    Inherits System.Web.UI.Page
    Public filename As String

    Public Function GetFilename(ByVal f As String) As String
        GetFilename = Path.GetFileName(f) ' .ToString.Substring(0, 7)
    End Function

    Public Function GetFileText(ByVal f As String) As String
        ' odd2010_09_27-13_26_57.pdf
        ' keno2010_09_27.pdf
        ' vlott2010_09_29.pdf
        ' jokrO2010_09_29.pdf
        ' tipM2010_09_28.pdf

        GetFileText = ""
        Try

            Dim FileName As String = Path.GetFileName(f)
            If LCase(FileName.Contains("pdf")) Then



                FileName = FileName.Replace(".pdf", "")

                ' We need to split the filename up in different pieces
                ' We also need to find out what part of the filename is characters
                ' and what part are numbers
                Dim FileNameParts As Array = FileName.Split("_")

                ' this means that 1 = filename + year
                ' We can remove the last four characters from the first one
                Dim FileNameGame As String = FileNameParts(0).ToString.Substring(0, FileNameParts(0).ToString.Length - 4)

                ' We then add the third one, which gives us the day in the month
                GetFileText = FileNameGame & FileNameParts(2) & ".pdf"
            End If
        Catch iex As IndexOutOfRangeException
            ' We really don't do anything here (yet)

        End Try

    End Function

    Public Function GetFileDate(ByVal f As String, ByVal fmt As String) As String
        GetFileDate = Format(File.GetLastWriteTime(f), fmt)
    End Function

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dlTippingM.DataSource = Directory.GetFiles(MapPath("bilder"), "TIPPM*.*")
        dlTippingM.DataBind()
        dlTippingL.DataSource = Directory.GetFiles(MapPath("bilder"), "TIPPL*.*")
        dlTippingL.DataBind()
        dlTippingS.DataSource = Directory.GetFiles(MapPath("bilder"), "TIPPS*.*")
        dlTippingS.DataBind()
        dlOddsen.DataSource = Directory.GetFiles(MapPath("bilder"), "ODD*.*")
        dlOddsen.DataBind()
        dlVLotto.DataSource = Directory.GetFiles(MapPath("bilder"), "VLOTT*.*")
        dlVLotto.DataBind()
        dlLotto.DataSource = Directory.GetFiles(MapPath("bilder"), "LOTTO*.*")
        dlLotto.DataBind()
        dlJoker.DataSource = Directory.GetFiles(MapPath("bilder"), "JOK*.*")
        dlJoker.DataBind()
        dlExtra.DataSource = Directory.GetFiles(MapPath("bilder"), "EXTRA*.*")
        dlExtra.DataBind()
        dlKeno.DataSource = Directory.GetFiles(MapPath("bilder"), "KENO*.*")
        dlKeno.DataBind()

    End Sub


End Class
