<%@ Outputcache Duration="900" Location="Any" VaryByParam="*"%>
<%@ Page EnableSessionState="false" Language="vb" AutoEventWireup="false" Codebehind="leftmenu.aspx.vb" Inherits="ntb.leftmenu"%>
<%@ Register TagPrefix="custom" TagName="leftmenu" src="leftmenu.ascx"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>NTB - Norsk Telegrambyrå</title>
	<link rel="stylesheet" href="css/styles.css">
</head>
<body bgcolor="#D1D5E3" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
			<table width="150" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff"><tr><td><img src="images/t.gif" height=4 width=1><br></td></tr></table>
			<table width="150" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="images/t.gif" height=1 width=12></td>
					<td valign=top>
						<img src="images/t.gif" height=3 width=1><br>
						<custom:leftmenu id="leftmenu" runat="server"></custom:leftmenu> 
					</td>
				</tr>
			</table>
</body>
</html>
