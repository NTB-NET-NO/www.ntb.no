<%@ Register TagPrefix="custom" TagName="internavis_list" src="printer-friendly_list.ascx"%>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="printer-friendly.aspx.vb" Inherits="ntb.printerfriendly"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>intern.ntb.no</title>
		<LINK href="css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<table width="500" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" align="left">
					<custom:internavis_list id="list" runat="server"></custom:internavis_list>
				</td>
			</tr>
		</table>
	</body>
</HTML>
