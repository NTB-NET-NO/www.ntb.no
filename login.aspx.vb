Imports ntb.Utils

Partial Class login
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents pMenu As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim maingroups As String

        If Page.IsPostBack = True Then
            maingroups = CheckLogin(txtUsername.Text, txtPassword.Text, Session("CurrentArticleMainGroup"), Request.UserHostAddress)

            If maingroups = "" And Session("CurrentArticleMainGroup2") <> "" Then
                maingroups = CheckLogin(txtUsername.Text, txtPassword.Text, Session("CurrentArticleMainGroup2"), Request.UserHostAddress)
            End If

            If maingroups <> "" Then
                Session("UserID") = txtUsername.Text

                AddUserHit(Session("UserID"), True, False, Session("SiteCode"))

                Dim arr As Array = Split(maingroups, ",")
                Dim i As Integer
                For i = 0 To arr.Length - 1
                    If arr(i) <> "" Then
                        Session("MainGroup" & CStr(arr(i))) = "1"
                    End If
                Next

                Server.Transfer(Session("TargetURL"))
            End If
        Else
            txtUsername.Text = Session("UserId")
        End If
    End Sub

End Class
