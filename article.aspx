<%@ Register TagPrefix="custom" TagName="leftmenu" src="leftmenu.ascx"%>
<%@ Page EnableSessionState="true" Language="vb" AutoEventWireup="false" Codebehind="article.aspx.vb" Inherits="ntb.article" enableViewState="False" codePage="1252"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>article</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=Windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="viewBody" id="body" runat="server">
		<table cellSpacing="0" cellPadding="0" width="436" border="0">
			<tr>
				<td vAlign="top">
					<img src="images/t.gif" height="4" width="1"><br>
					<form id="frmArticle" method="post" runat="server">
						<asp:Panel id="pArticle" runat="server">
							<asp:xml id="H" runat="server" EnableViewState="False"></asp:xml>
							<asp:xml id="U1" runat="server" EnableViewState="False"></asp:xml>
							<asp:xml id="U2" runat="server" EnableViewState="False"></asp:xml>
							<asp:xml id="U3" runat="server" EnableViewState="False"></asp:xml>
							<asp:xml id="U4" runat="server" EnableViewState="False"></asp:xml>
							<asp:xml id="U5" runat="server" EnableViewState="False"></asp:xml>
						</asp:Panel>
						<asp:Panel id="pGifArticle" runat="server">
							<BR>
							<asp:Image id="imgArticle" runat="server"></asp:Image>
							<BR>
							<BR>
							<asp:Image id="imgArticleU1" runat="server" Visible="False"></asp:Image>
							<asp:Image id="imgArticleU2" runat="server" Visible="False"></asp:Image>
							<asp:Image id="imgArticleU3" runat="server" Visible="False"></asp:Image>
							<asp:Image id="imgArticleU4" runat="server" Visible="False"></asp:Image>
							<asp:Image id="imgArticleU5" runat="server" Visible="False"></asp:Image>
							<asp:Xml id="Pictures" runat="server" EnableViewState="False" Visible="False"></asp:Xml>
							<HR SIZE="1">
							<DIV class="artikkel_tekst"><B>All nedlasting, enten det er bilder eller tekst, tolkes 
									som bruk og vil bli fakturert.</B></DIV>
						</asp:Panel>
						<div align="right"><p>
								<asp:hyperlink cssClass="artikkel_liste_dato_tid" id="hLink" runat="server" EnableViewState="False"></asp:hyperlink><asp:label cssClass="artikkel_liste_dato_tid" id="lblDownload" runat="server" EnableViewState="False"></asp:label></p>
						</div>
					</form>
				</td>
			</tr>
		</table>
		<p></p>
	</BODY>
</HTML>
