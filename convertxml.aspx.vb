Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls

Partial Class convertxml
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dbNTB As New SqlConnection(ConfigurationManager.AppSettings("conString"))
        Dim cd As New SqlCommand
        Dim dr As SqlDataReader
        Dim xslt, savefilename, keyword As String
        Dim refid As Integer = CInt(Request.QueryString("RefID"))
        Dim maingroup, i As Integer
        Dim output As String = ""
        Dim tmp As String
        Dim HasSubArticles As Boolean = False

        xslt = Request.QueryString("XSLT")
        If xslt <> "" Then
            savefilename = CStr(refid) & "."
            If xslt.IndexOf("/") <> -1 Then
                savefilename += xslt.Substring(xslt.IndexOf("/") + 1, xslt.IndexOf(".") - xslt.IndexOf("/") - 1)
            Else
                savefilename += xslt.Substring(1, xslt.IndexOf(".") - 1)
            End If
        Else
            savefilename = CStr(refid) & ".xml"
        End If

        cd.CommandText = "_GetArticle"
        cd.CommandType = CommandType.StoredProcedure
        cd.Parameters.Add(New SqlParameter("@refid", SqlDbType.Int))
        cd.Parameters.Item("@refid").Value = refid
        cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.Int))
        cd.Parameters.Item("@maingroup").Value = CInt(Session("CurrentArticleMainGroup"))
        cd.Connection = dbNTB
        dbNTB.Open()
        dr = cd.ExecuteReader
        If dr.Read Then
            maingroup = dr("Maingroup")
            keyword = dr("Keyword")
            If xslt <> "" Then
                H.TransformSource = xslt
                H.DocumentContent = dr("ArticleXML")
                Response.ContentType = "text/text; charset=iso-8859-1"
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1252")
            Else
                output = dr("ArticleXML")
            End If
            dr.Close()

            If False And keyword.Length > 3 Then ' finn underartikler (feature)
                cd.CommandText = "select articlexml, keyword, Refid, HasPictures from Articles where keyword like 'U_-" & keyword.Substring(0).Replace("'", "''") & "' order by Keyword"
                cd.CommandType = CommandType.Text
                dr = cd.ExecuteReader
                For i = 1 To 5
                    Dim n As String = "U" & CStr(i)
                    Dim xml As System.Web.UI.WebControls.Xml
                    'Dim img As System.Web.UI.WebControls.Image
                    xml = Page.FindControl(n)
                    If dr.Read Then
                        HasSubArticles = True
                        If xslt <> "" Then
                            xml.DocumentContent = dr("ArticleXML")
                            xml.TransformSource = xslt
                        Else
                            tmp = dr("ArticleXML")
                            tmp = tmp.Replace("<nitf", "<nitf-sub" & CStr(i))
                            tmp = tmp.Replace("</nitf>", "</nitf-sub" & CStr(i) & ">")
                            output += tmp
                        End If
                    End If
                Next
                dr.Close()
            End If
        End If
        If output <> "" Then
            If HasSubArticles Then
                output = output.Replace("<nitf ", "<articles>" & vbNewLine & "<nitf ")
                output += vbNewLine & "</articles>"
            End If
            Response.ContentType = "text/xml"
            Response.Write(output)
        End If
        Response.AppendHeader("Content-disposition", "inline; filename=" & savefilename)
        If Session("UserId") <> "" Then SaveDLStats(Session("UserId"), refid, maingroup, 1, 0)
        dbNTB.Close()
    End Sub

End Class
