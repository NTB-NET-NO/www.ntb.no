<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes"/>

<!-- 
	Stylesheet used to display only the pictures on an article
-->

<xsl:template match="/nitf">
<html>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
<body class='viewBody'>
	
	<xsl:if test="count(body/body.content/media/media-reference/@source[. != '']) &gt;= 1">
	<div><p></p></div>
		<hr size="1" />
		<table>
			<!--xsl:apply-templates select="body/body.content/media[@media-type='image' and (not(@class) or @class!='prm')]"/-->
			<xsl:apply-templates select="body/body.content/media[media-reference/@source != '']"/>
		</table>
	</xsl:if>

</body>
</html>
</xsl:template>


<!-- Templates -->

<xsl:template match="body/body.content/media[@media-type='image' and (not(@class) or @class!='prm')]">
	<!-- Template for Scanpix media	-->
	<tr>
		<td>
			<a target="_blank"><xsl:attribute name="href">picture.aspx?Type=L&amp;SpCode=<xsl:value-of select="media-reference/@source"/></xsl:attribute>
			<img border="0"><xsl:attribute name="src">http://80.91.34.200/cgi-bin/picture?/4/UNKNOWN/Q_<xsl:value-of select="media-reference/@source"/></xsl:attribute></img>
			</a>
		</td>
		<td valign="top">
			<div class="viewIngress">Bilde: <xsl:value-of select="media-reference/@source"/></div>
			<div class="viewNews"><xsl:value-of select="media-caption"/></div>
			<div class="viewNews"><a target="_blank"><xsl:attribute name="href">picture.aspx?Type=H&amp;SpCode=<xsl:value-of select="media-reference/@source"/></xsl:attribute>Last ned h�yoppl�selig</a></div>
		</td>
	</tr>	
</xsl:template>

<!-- Template for local attachments i.e. PDF -->
<xsl:template match="body/body.content/media[@media-type='other' and media-reference/@mime-type='application/pdf' and @class='tema']">
	<tr>
		<td align="center">
			<a target="_blank"><xsl:attribute name="href">picture.aspx?attachment=<xsl:value-of select="media-reference/@source"/></xsl:attribute>
			<img border="0" src="images/pdficon_large.gif" />
			</a>
		</td>
		<td valign="top">
			<div class="viewIngress">Vedlegg: <xsl:value-of select="media-reference/@source"/></div>
			<div class="viewNews"><xsl:value-of select="media-caption"/></div>
			<div class="viewNews"><a target="_blank"><xsl:attribute name="href">picture.aspx?attachment=<xsl:value-of select="media-reference/@source"/></xsl:attribute>Last ned vedlegg</a></div>
		</td>
	</tr>	
</xsl:template>

</xsl:stylesheet>