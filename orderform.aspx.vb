Imports ntb.Utils

Partial Class orderform
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Produkt As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Produktet.Items.Add(Request.QueryString("Section"))
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim Body, Subject, From, ToAdd, ItemName, ItemValue As String
        Dim i As Integer
        Subject = Request("__Subject")
        From = Request("__From")
        ToAdd = Request("__To")
        If ToAdd = "" Then ToAdd = ConfigurationManager.AppSettings("DefaultOrderformEmail")
        Body = Subject + vbNewLine + vbNewLine
        For i = 0 To Request.Form.Count - 1
            ItemName = Request.Form.AllKeys(i)
            If ItemName <> "__VIEWSTATE" And ItemName <> "__SUBJECT" And ItemName <> "__TO" And ItemName <> "__FROM" And ItemName <> "__TITLE" And ItemName <> "btnSubmit" Then
                ItemValue = Request.Form.GetValues(i)(0)
                Body += (ItemName & " : " & ItemValue & vbNewLine)
            End If
        Next
        If SendEmail(From, ToAdd, Subject, Body) Then
            Server.Transfer("orderformok.aspx?section=" & Request.QueryString("Section"))
        Else
            Server.Transfer("orderformfailed.aspx?section=" & Request.QueryString("Section"))
        End If
    End Sub
End Class
