Imports System.Data.SqlClient
Imports ntb.Utils

Partial Class forgotpassword
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pMenu As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack Then
            lblEmail.Visible = False
            Page.Validate()
            If cvUserId.IsValid Then
                Dim userid As String = txtUsername.Text
                Dim email As String = GetUserData(userid, "Email")
                If email <> "" Then
                    If Not SendEmail(ConfigurationManager.AppSettings("ForgotPasswordFrom"), email, "Glemt passord?", "Passordet p� din konto hos NTB er: " & GetUserData(userid, "Password")) Then
                        lblEmail.Text = "Kunne ikke sende e-post!"
                        lblEmail.Visible = True
                    Else
                        lblEmail.Text = "E-post med passord sendt til " & email
                        lblEmail.Visible = True
                    End If
                Else
                    lblEmail.Text = "Det mangler e-post adresse p� denne kontoen!"
                    lblEmail.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvUserId.ServerValidate
        args.IsValid = (GetUserData(txtUsername.Text, "Password") <> "")
    End Sub
End Class
