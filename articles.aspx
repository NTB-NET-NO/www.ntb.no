<%@ Page EnableSessionState="ReadOnly" Language="vb" AutoEventWireup="false" Codebehind="articles.aspx.vb" Inherits="ntb.articles" codePage="1252"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>articles</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="True" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=Windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="css/styles.css" type="text/css" rel="stylesheet">
		<META http-equiv="Refresh" content="300">
		<style type="text/css">
		
		A { FONT-SIZE: 11px; COLOR: #5161ac; FONT-FAMILY: arial; TEXT-DECORATION: none }
		
		A:visited { COLOR: #000000; TEXT-DECORATION: none }
		
		A:hover { TEXT-DECORATION: underline }
		
		A:active { COLOR: #990000; TEXT-DECORATION: none }
		
		</style>
		<!--
		<style type=text/css>
				a {			font-family: arial;			font-size: 11px;			color: #5161AC;			text-decoration: none;		}
		a:visited {			color: #000000;			text-decoration: none;		}		a:hover {			color: #bb0000;			text-decoration: underline;		}		a:active {			color: #bb0000;			text-decoration: underline;		}	
		</style>
		-->
	</HEAD>
	<body>
		<form id="frmArticles" method="post" runat="server">
			<table width="167" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="images/t.gif" height="1" width="3"></td>
					<td>
						<img src="images/t.gif" height="4" width="1"><br>
						<asp:label id="lHeader" runat="server"></asp:label><br>
						<img src="images/t.gif" height="1" width="1"><br>
						<!-- <form action="" style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px"> -->
						<asp:panel id="pSearch" runat="server" Height="25px" Visible="False">
							<TABLE cellSpacing="0" cellPadding="0" width="167" bgColor="#5c76b0" border="0">
								<TR>
									<TD align="right" width="71"><IMG height="12" alt="S�k i artikler" src="images/artikler/sok_tekst.gif" width="25"
											border="0">&nbsp;</TD>
									<TD align="center" height="25">
										<asp:TextBox id="txtSearch" style="FONT-SIZE: 9px; FONT-FAMILY: arial" runat="server" Height="15"
											Width="98"></asp:TextBox></TD>
									<TD align="left" width="71">&nbsp;
										<asp:ImageButton id="imgbtnSearch" runat="server" ImageUrl="images/artikler/sok_button.gif"></asp:ImageButton></TD>
								</TR>
							</TABLE>
						</asp:panel>
						<!-- </form> -->
						<table width="167" border="0" cellspacing="0" cellpadding="0">
							<tr id="hdr" runat="server" visible="false">
								<TD colspan="2" class="artikkel_liste_dato_tid">Ingen artikler funnet!</TD>
							</tr>
							<tr>
								<!-- <td><img src="images/t.gif" height="1" width="3"><br></td> -->
								<td width="159" valign="top">
									<asp:datalist id="dlArticles" runat="server" ShowFooter="False" EnableViewState="False" CellPadding="0"
										CellSpacing="0">
										<ItemTemplate>
											<tr>
												<td colspan="2" class="artikkel_liste_dato_tid"><%# dateheader%></td>
											</tr>
											<tr>
												<td valign="top"><font class="artikkel_liste_dato_tid" color='<%# alertcolor%>'><%# Databinder.Eval(Container.DataItem, "Creationdatetime", "{0:t}")%>&nbsp;</font></td>
												<td valign="top"><a href='article.aspx?Section=<%# section%>&amp;RefID=<%#dr("RefId")%>&amp;MainGroup=<%# dr("MainGroup")%>' target=mainart><%#dr("ArticleTitle")%><%# picTag %></a></td>
											</tr>
										</ItemTemplate>
									</asp:datalist>
									<div align="right"><p><asp:HyperLink id="hArchive" runat="server" CssClass="artikkel_liste_dato_tid"></asp:HyperLink></p>
									</div>
								</td>
								<!-- <td valign="top" width="3"><img src="images/t.gif" height="1" width="3"></td> -->
							</tr>
						</table>
						</B>
					</td>
				</tr>
			</table>
		</form>
		<p></p>
	</body>
</HTML>
