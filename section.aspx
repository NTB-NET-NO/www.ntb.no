<%@ Register TagPrefix="custom" TagName="ticker" src="ticker.ascx"%>
<%@ Page EnableSessionState="true" Language="vb" AutoEventWireup="false" Codebehind="section.aspx.vb" Inherits="ntb.section" codePage="1252"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Section</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=Windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY id="body" runat="server">
		<table width="436" border="0" cellspacing="0" cellpadding="0" height="99%">
			<tr>
				<td valign="top">
					<table width="436" border="0" cellspacing="0" cellpadding="0" runat="server" id="mainTable">
						<tr>
							<td>
								<img src="images/t.gif" height="3" width="1"><br>
								<asp:Table id="topTbl" width="100%" cellpadding="0" cellspacing="0" Runat="server">
									<asp:tablerow>
										<asp:tablecell>
											<asp:Image id="imgTitleGif" Runat="server"></asp:Image>
										</asp:tablecell>
									</asp:tablerow>
								</asp:Table>
								<!-- CONTENT -->
								<asp:Panel ID="pHTMLContent" Runat="server" Visible="false">
									<asp:label id="lblContent" runat="server"></asp:label>
								</asp:Panel>
								<P><asp:label id="lblTitle" runat="server" cssclass="artikkel_header"></asp:label></P>
								<P><b><asp:label id="lblIngress" runat="server" cssclass="artikkel_tekst"></asp:label></b></P>
								<P><asp:label id="lblText" runat="server" CssClass="artikkel_tekst"></asp:label></P>
								<!-- FOOTER -->
								<asp:panel id="pFooter" runat="server" Visible="False">
									<HR color="#7b7b7b" SIZE="1">
									<asp:Label id="lblFooter" runat="server" cssclass="artikkel_bottomtekst"></asp:Label>
								</asp:panel>
								<!-- /FOOTER -->
								<P></P>
								<!-- /CONTENT -->
							</td>
							<TD vAlign="top" rowspan="3" height="100%">
								<!-- BILDE -->
								<asp:Panel ID="pPicture" Runat="server">
									<TABLE height="100%" cellSpacing="0" cellPadding="0" width="200" align="right" border="0">
										<TR>
											<TD vAlign="top"><IMG height="1" src="images/t.gif" width="3"></TD>
											<TD vAlign="top" width="200" bgColor="#d1d5e3">
												<TABLE cellSpacing="0" cellPadding="0" width="200" bgColor="#ffffff" border="0">
													<TR>
														<TD><IMG height="4" src="images/t.gif" width="1"><BR>
														</TD>
													</TR>
												</TABLE>
												<IMG height="50" src="images/t.gif" width="1"><BR>
												<TABLE cellSpacing="0" cellPadding="0" width="200" bgColor="#ffffff" border="0">
													<TR>
														<TD><IMG height="2" src="images/t.gif" width="1"><BR>
														</TD>
													</TR>
												</TABLE>
												<DIV class="artikkel_tekst"><B>
														<asp:label id="lblPictureHeading" runat="server"></asp:label></B></DIV>
												<asp:image id="imgSection" runat="server"></asp:image>
												<DIV class="artikkel_tekst">
													<asp:label id="lblPictureText" Runat="server"></asp:label></DIV>
												<TABLE cellSpacing="0" cellPadding="0" width="200" bgColor="#ffffff" border="0">
													<TR>
														<TD><IMG height="2" src="images/t.gif" width="1"><BR>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</asp:Panel>
								<!-- /BILDE -->
							</TD>
						</tr>
						<tr>
							<td>
								<!-- SIST ENDRET -->
								<asp:panel ID="pLastModified" Runat="server" Visible="false">
									<TABLE>
										<TR>
											<TD class="artikkel_bottomtekst" colSpan="2">Sist endret:&nbsp;
												<asp:label id="lblLastModified" runat="server"></asp:label></TD>
										</TR>
									</TABLE>
								</asp:panel>
								<!-- /SIST ENDRET -->
							</td>
						</tr>
					</table>
					<!-- /HOVEDFELT -->
				</td>
				<td valign="top"><img src="images/t.gif" height="1" width="3"></td>
				<td bgColor="#d1d5e3" valign="top">
					<!-- MIDTFELT (BILDER) -->
					<!-- FLASH -->
					<asp:panel ID="pPictureHTML" Runat="server" Visible="false">
						<TABLE cellSpacing="0" cellPadding="0" width="133" border="0">
							<TR>
								<TD vAlign="top" width="133" bgColor="#d1d5e3">
									<TABLE cellSpacing="0" cellPadding="0" width="133" bgColor="#ffffff" border="0">
										<TR>
											<TD><IMG height="4" src="images/t.gif" width="1"><BR>
											</TD>
										</TR>
									</TABLE>
									<IMG height="50" src="images/t.gif" width="1"><BR>
									<TABLE cellSpacing="0" cellPadding="0" width="133" bgColor="#ffffff" border="0">
										<TR>
											<TD><IMG height="2" src="images/t.gif" width="1"><BR>
											</TD>
										</TR>
									</TABLE>
									<asp:Label id="lblPictureHTML" Runat="server"></asp:Label>
									<TABLE cellSpacing="0" cellPadding="0" width="133" bgColor="#ffffff" border="0">
										<TR>
											<TD><IMG height="2" src="images/t.gif" width="1"><BR>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</asp:panel>
					<!-- /FLASH -->
					<!-- /MIDTFELT (BILDER) -->
				</td>
			</tr>
		</table>
	</BODY>
</HTML>
