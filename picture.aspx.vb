Imports ntb.Utils
Imports System.Net
Imports System.IO
Imports System.Data.SqlClient

Partial Class picture
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents scanpix As System.Web.UI.WebControls.Image

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub CopyData(ByVal FromStream As Stream, ByVal ToStream As Stream)
        Dim intBytesRead As Integer
        Const intSize As Integer = 4096
        Dim bytes(intSize) As Byte

        intBytesRead = FromStream.Read(bytes, 0, intSize)
        While intBytesRead > 0
            ToStream.Write(bytes, 0, intBytesRead)
            intBytesRead = FromStream.Read(bytes, 0, intSize)
        End While
    End Sub

    Private Sub DownloadAttachment()
        Dim url, prefix, filename As String

        prefix = Request.QueryString("class")
        filename = Request.QueryString("attachment")

        url = ConfigurationManager.AppSettings("attachmenturl")

        If (prefix <> "") Then
            url &= prefix & "/"
        End If

        url &= System.Web.HttpUtility.UrlEncode(filename)
        url = url.Replace("+", "%20")

        Dim uri As New Uri(url)
        Dim req As WebRequest
        Dim fs As FileStream
        Dim fname As String = MapPath("temp") & "\" & filename
        If Not File.Exists(fname) Then
            fs = New FileStream(fname, FileMode.Create)
            Try
                req = WebRequest.Create(uri)
                req.Method = "GET"
                Dim rsp As WebResponse = Nothing
                Try
                    rsp = req.GetResponse()
                    CopyData(rsp.GetResponseStream(), fs)
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not fs Is Nothing Then fs.Close()
                    If Not rsp Is Nothing Then rsp.GetResponseStream.Close()
                End Try
            Catch
            End Try
        End If

        Dim referer As String = Request.ServerVariables("HTTP_REFERER")

        If Not referer Is Nothing Then
            If referer.IndexOf("portal.ntb.no") > -1 Then
                referer = "portalen"
            Else
                referer = ""
            End If
        Else
            referer = ""
        End If

        If referer = "" Then
            If Session("UserID") = "" Then
                Session("TargetURL") = Request.RawUrl
                Server.Transfer("login.aspx")
            Else
                SaveDLStats(Session("UserID"), Session("CurrentArticleId"), Session("CurrentArticleMaingroup"), 0, 1)
            End If
        End If

        Response.AppendHeader("Content-disposition", "inline; filename=" & Path.GetFileName(fname))
        Response.ContentType = "application/pdf"
        Response.WriteFile(fname)
    End Sub

    Private Sub DownloadImage()
        Dim tp, url, spcode As String
        tp = Request.QueryString("Type").ToUpper
        spcode = Request.QueryString("spcode")
        url = ConfigurationManager.AppSettings("scanpixurl" & tp) & spcode

        Dim uri As New Uri(url)
        Dim req As WebRequest
        Dim fs As FileStream
        Dim fname As String = MapPath("temp") & "\" & spcode & tp & ".jpg"
        If Not File.Exists(fname) Then
            fs = New FileStream(fname, FileMode.Create)
            Try
                req = WebRequest.Create(uri)
                req.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("scanpixuserid"), ConfigurationManager.AppSettings("scanpixpassword"))
                req.Method = "GET"
                Dim rsp As WebResponse = Nothing
                Try
                    rsp = req.GetResponse()
                    CopyData(rsp.GetResponseStream(), fs)
                Catch
                Finally
                    If Not fs Is Nothing Then fs.Close()
                    If Not rsp Is Nothing Then rsp.GetResponseStream.Close()
                End Try
            Catch
            End Try
        End If

        Dim referer As String = Request.ServerVariables("HTTP_REFERER")

        If Not referer Is Nothing Then
            If referer.IndexOf("portal.ntb.no") > -1 Then
                referer = "portalen"
            Else
                referer = ""
            End If
        Else
            referer = ""
        End If

        If referer = "" Then
            If Session("UserID") = "" Then
                Session("TargetURL") = Request.RawUrl
                Server.Transfer("login.aspx")
            Else
                SaveDLStats(Session("UserID"), Session("CurrentArticleId"), Session("CurrentArticleMaingroup"), 0, 1)
            End If
        End If

        Response.AppendHeader("Content-disposition", "inline; filename=" & Path.GetFileName(fname))
        'Response.ContentType = "image/jpg"
        Response.WriteFile(fname)
    End Sub

    Private Sub ShowDBImage()
        Dim dbNTB As New SqlConnection(ConfigurationManager.AppSettings("conString"))
        Dim cd As New SqlCommand("select Picture from Sections where SiteCode = '" & Session("SiteCode") & "' and Name = '" & Request.QueryString("Section") & "'", dbNTB)
        Dim dr As SqlDataReader

        dbNTB.Open()
        dr = cd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            Response.ContentType = "image/jpg"
            If dr.Read Then Response.BinaryWrite(dr.Item("Picture"))
        Catch
        End Try
        dr.Close()
    End Sub

    Private Sub ShowFileImage()
        Dim file As String = MapPath("/") & Request.QueryString("File")
        Dim Image() As Byte
        Dim stream As System.IO.FileStream
        Dim bytecount As Integer

        stream = New System.IO.FileStream(file, FileMode.Open)
        bytecount = stream.Length

        ReDim Image(bytecount - 1)

        Response.BinaryWrite(Image)
        Response.ContentType = "image/jpg"
        stream.Close()
        stream = Nothing
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Request.QueryString("File") <> "" Then
            If Request.QueryString("Save") = "1" Then
                SaveDBImage(MapPath("/") & Request.QueryString("File"), Request.QueryString("Section"), "", Session("SiteCode"))
            Else
                ShowFileImage()
            End If
            Exit Sub
        End If
        If Request.QueryString("attachment") <> "" Then
            DownloadAttachment()
        ElseIf Request.QueryString("spcode") <> "" Then
            DownloadImage()
        Else
            ShowDBImage()
        End If
    End Sub

End Class
