<%@ Page Language="vb" AutoEventWireup="false" Codebehind="forgotpassword.aspx.vb" Inherits="ntb.forgotpassword"%>
<%@ Register TagPrefix="custom" TagName="leftmenu" src="leftmenu.ascx"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>forgotpassword</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body id="body" runat=server>
		<form id="frmForgotPassword" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="436" border="0">
				<tr>
					<td vAlign="top"><br>
						<div class="artikkel_tekst">Hvis du har glemt passordet kan du skrive inn brukernavnet 
							under.<br>
							Passordet vil da sendes p� e-post til den adressen kontoen er registert p�.<br>
							Hvis det ikke er registert noen e-post adresse p� kontoen ta kontakt med <a href="mailto:marked@ntb.no">marked@ntb.no</a>
						</div>
						<br>
						<br>
						<table>
							<tr>
								<td vAlign="top"><asp:label id="Label2" runat="server" CssClass="artikkel_liste_dato_tid">Brukernavn: </asp:label></td>
								<td valign=top><asp:textbox id="txtUsername" runat="server"></asp:textbox>&nbsp;<asp:button id="btnLogin" runat="server" cssclass="formbutton" Text="Send passord"></asp:button></td>
							</tr>
							<tr>
								<td></td>
								<td><asp:label id="lblEmail" runat="server" Visible="False" CssClass="artikkel_liste_dato_tid" ForeColor="Red">E-post med passord sendt!</asp:label><asp:customvalidator id="cvUserId" runat="server" cssclass="artikkel_liste_dato_tid" ErrorMessage="Brukernavnet finnes ikke i databasen!"></asp:customvalidator></td>
							</tr>
							<!--<tr>
								<td></td>
								<td class="b_txt">Tilbake til <a href="javascript:history.go(-1);">login-bildet</a>.</td>
							</tr> -->
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
