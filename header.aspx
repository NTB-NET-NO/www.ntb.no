<%@ Page EnableSessionState="false" Language="vb" AutoEventWireup="false" Codebehind="header.aspx.vb" Inherits="ntb.header2"%>
<%@ Outputcache Duration="900" Location="Any" VaryByParam="*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
	<title>NTB - Norsk Telegrambyr�</title>
	<link rel="stylesheet" href="css/styles.css">

<script type="text/javascript" src="includes/overlib/overlibmws.js"></script>
<script type="text/javascript" src="includes/overlib/overlibmws_hide.js"></script>
<script type="text/javascript" src="includes/overlib/overlibmws_iframe.js"></script>
<script type="text/javascript" src="includes/overlib/overlibmws_shadow.js"></script>

<script language="javascript">
<!--

var ol_vpos = VCENTER;
var ol_width= 230;
var ol_hidebyidall = 'flashDiv';

var ol_fgcolor = '#FFFFFF';
var ol_bgcolor = '#E16425';
var ol_cgcolor = '#E16425';

var ol_textcolor = '#000000';
var ol_capcolor = '#FFFFFF';

var ol_textsize = '10px'
var ol_captionsize = '10px'

//-->
</script>
</HEAD>

<body bgcolor="#ffffff" leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
<div id="overDiv" style="Z-INDEX:1000; VISIBILITY:hidden; POSITION:absolute"></div>

<img src="images/t.gif" height=4 width=1><br>

<SCRIPT LANGUAGE="JavaScript">
<!--
var MM_contentVersion = 6;
var plugin = (navigator.mimeTypes && navigator.mimeTypes["application/x-shockwave-flash"]) ? navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin : 0;
if ( plugin ) {
		var words = navigator.plugins["Shockwave Flash"].description.split(" ");
	    for (var i = 0; i < words.length; ++i)
	    {
		if (isNaN(parseInt(words[i])))
		continue;
		var MM_PluginVersion = words[i]; 
	    }
	var MM_FlashCanPlay = MM_PluginVersion >= MM_contentVersion;
}
else if (navigator.userAgent && navigator.userAgent.indexOf("MSIE")>=0 
   && (navigator.appVersion.indexOf("Win") != -1)) {
	document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n'); //FS hide this from IE4.5 Mac by splitting the tag
	document.write('on error resume next \n');
	document.write('MM_FlashCanPlay = ( IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash." & MM_contentVersion)))\n');
	document.write('</SCR' + 'IPT\> \n');
}
if ( MM_FlashCanPlay ) {
		var oeTags = '<div id="flashDiv" style="position:relative; width:100%; z-index:1;"><OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="800" HEIGHT="60" id="topp" ALIGN=""><PARAM NAME=movie VALUE="images/swf/topp03.swf"><PARAM NAME=quality VALUE=high><PARAM NAME=bgcolor VALUE=#FFFFFF><EMBED src="images/swf/topp03.swf" quality=high bgcolor=#FFFFFF  WIDTH="800" HEIGHT="60" NAME="topp" ALIGN="" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED></OBJECT></div>';
		document.write(oeTags); 	// embed the flash movie
} else{
		var alternateContent = '<img src="images/swf/flash.gif" width="800" height="60" alt="" border="0">'	// height, width required!
		document.write(alternateContent);	// insert non-flash content
}
//-->
</SCRIPT>

<img src="images/t.gif" height=4 width=1><br>
<table width="800" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign=top width=150 height=15 bgcolor="#e16425"><A class=top_menu_white onmouseover="return overlib('Vaktsjef: 22 03 45 45<BR>Sporten: 22 03 45 55<BR>Utenriks: 22 03 45 50<a href="section.aspxSection='TIPSNTB"' target=main onmouseout="return nd();"?>&nbsp;&nbsp;Tips NTB</A></FONT></td>
		<td valign=top width=5><img src="images/t.gif" height=1 width=5></td>
		<td valign=top width=300>
			<table width="300" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width=67 bgcolor="#445da0"><a class="top_menu_white" href="section.aspx?Section=OMNTB" target=main>&nbsp;Om NTB</a></FONT></td>
					<td><img src="images/topmenu_skille1.gif" width="14" height="15" alt="" border="0"><br></td>
					<td width=58 bgcolor="#7584b3"><a class="top_menu_white" href="section.aspx?Section=SPRAAK" target=main>&nbsp;Spr�k</a></FONT></td>
					<td><img src="images/topmenu_skille2.gif" width="14" height="15" alt="" border="0"><br></td>
					<td width=66 bgcolor="#445da0"><a class="top_menu_white" href="http://www.riksnytt.no" target="_blank">&nbsp;Riksnytt</FONT></a></td>
					<td><img src="images/topmenu_skille3.gif" width="14" height="15" alt="" border="0"></td>
					<td width=53 bgcolor="#7584b3"><a class="top_menu_white" href="http://www.npk.no/" target="_top">&nbsp;NPK</a></td>
					<td><img src="images/topmenu_skille4.gif" width="14" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
		<td valign=top width=3><img src="images/t.gif" height=1 width=3></td>
		<td valign=top width=342>
			<table width="342" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="97" background="images/topmenu_sistenbt_bgimg.gif"><font class="top_menu_black">&nbsp;&nbsp;Siste nytt</font></td>
					<td width=12><img src="images/topmenu_skille5.gif" width="12" height="15" alt="" border="0"></td>
					<td width="101" background="images/topmenu_flerenbt_bgimg.gif"><font class="top_menu_white">&nbsp;&nbsp;<a class="top_menu_white" href="<% Response.Write(direktelink) %>" target="_new">Flere nyheter</a></font></td>
					<td><img src="images/topmenu_skille6.gif" width="12" height="15" alt="" border="0"></td>
					<td width="69" background="images/topmenu_sistenbt_bgimg_2.gif"><a class="leftmenu" href="section.aspx?Section=ABOUTNTB" target=main>&nbsp;English</FONT></a></td>
					<td width=12><img src="images/topmenu_skille5.gif" width="12" height="15" alt="" border="0"></td>
					<td width="69" background="images/topmenu_flerenbt_bgimg.gif"><a class="leftmenu" href="section.aspx?Section=KONTAKT" target=main>&nbsp;Kontakt 
            NTB</FONT></a></td>
					<td width="1" bgcolor="#bbbbbb"><img src="images/t.gif" height=1 width=1></td>

					<!--
					<td width="186" background="images/topmenu_flerenbt_bgimg.gif"><font class="top_menu_white">&nbsp;&nbsp;<a class="leftmenu" href="<% Response.Write(direktelink) %>" target="_new" onmouseover="return overlib('Klikk her for flere nyheter fra NTB. Linken peker til nyhetsoversikten til en av de store kundene. Innholdet er levert av NTB.', CAPTION, 'Flere nyheter fra NTB', LEFT) ;" onmouseout="return nd();">Flere NTB-nyheter</a></font></td>
					-->
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</HTML>
