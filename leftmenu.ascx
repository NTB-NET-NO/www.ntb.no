<%@ Control Language="vb" AutoEventWireup="false" Codebehind="leftmenu.ascx.vb" Inherits="ntb.leftmenu2" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Outputcache Duration="30" VaryByParam="section"%>
<!-- this is the menu -->
<asp:datalist CSSClass="artikkel_tekst" id="dlMenu" runat="server" CellPadding="0" CellSpacing="0"
	RepeatLayout="Table">
	<ItemTemplate>
		<asp:label ID=label Visible=false runat=server>
			<%# lmdr("LinkName") %>
		</asp:label>
		<asp:label ID=link Visible=false runat=server>
			<A class=leftmenu href='<%# lmdr("URL") %>' target='<%# lmdr("Target") %>'><%# lmdr("LinkName") %></A>&nbsp;<FONT color="red"><B><%# lmdr("IsNewText")%></B></FONT>
		</asp:label>
	</ItemTemplate>
</asp:datalist>
<!-- end menu 

		<% if not lmdr.isclosed then
			if lmdr("URL") <> "" then %>

			<% else %>
			<%# lmdr("LinkName") %>
			<% end if %>
		<% end if %>

-->
