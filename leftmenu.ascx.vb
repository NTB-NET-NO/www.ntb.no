Imports System.Data.SqlClient
Partial Class leftmenu2
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "
    Protected WithEvents dlSubMenu As System.Web.UI.WebControls.DataList

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected lmdr As SqlDataReader
    Protected subitems As String = ""
    Protected submenus As New ArrayList

    Private Sub BindMenu(ByVal menuid As Integer, ByVal menu As DataList)
        Dim dbNTB As New SqlConnection(ConfigurationManager.AppSettings("conString"))
        Dim cd As New SqlCommand

        cd.Connection = dbNTB
        dbNTB.Open()
        cd.CommandType = CommandType.StoredProcedure
        cd.CommandText = "_GetMenu"
        cd.Parameters.Clear()
        cd.Parameters.Add(New SqlParameter("@menuid", SqlDbType.Int))
        cd.Parameters.Item("@menuid").Value = menuid
        cd.Parameters.Add(New SqlParameter("@newtext", SqlDbType.VarChar, 255))
        cd.Parameters.Item("@newtext").Value = ConfigurationManager.AppSettings("newtext")
        cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
        cd.Parameters.Item("@sitecode").Value = ConfigurationManager.AppSettings("SiteCode")
        lmdr = cd.ExecuteReader
        menu.DataSource = lmdr
        menu.DataBind()
        lmdr.Close()
        dbNTB.Close()
    End Sub

    Private Function BindSubMenu(ByVal menuid As Integer) As String
        Dim dbNTB As New SqlConnection(ConfigurationManager.AppSettings("conString"))
        Dim cd As New SqlCommand
        Dim dr As SqlDataReader
        Dim m As String

        cd.Connection = dbNTB
        dbNTB.Open()
        cd.CommandType = CommandType.StoredProcedure
        cd.CommandText = "_GetMenu"
        cd.Parameters.Clear()
        cd.Parameters.Add(New SqlParameter("@menuid", SqlDbType.Int))
        cd.Parameters.Item("@menuid").Value = menuid
        cd.Parameters.Add(New SqlParameter("@newtext", SqlDbType.VarChar, 255))
        cd.Parameters.Item("@newtext").Value = ConfigurationManager.AppSettings("newtext")

        m = "<div id='sub" & menuid & "' style='Z-INDEX:1; VISIBILITY:hidden; WIDTH:250px; POSITION:absolute'>"
        m += "<table class='submenu' cellpadding='0' cellspacing='0' width='120'>"
        dr = cd.ExecuteReader
        While dr.Read
            m += "<tr><td><a href='" & dr("URL") & "' target='" & dr("Target") & "'>" & dr("LinkName") & "</a>&nbsp;<font color='red'><b>" & dr("IsNewText") & "</b></td></tr>"
        End While
        m += "</table></div>"
        BindSubMenu = m
    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim i As Integer
        If Not Page.IsPostBack Then
            BindMenu(1, dlMenu)
            For i = 0 To submenus.Count - 1
                subitems += BindSubMenu(submenus(i))
            Next
        End If
    End Sub

    Private Sub dlMenu_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlMenu.ItemCreated
        Try
            If CType(sender, DataList).DataSource("SubMenu") > 0 Then submenus.Add(CType(sender, DataList).DataSource("SubMenu"))
        Catch
        End Try
    End Sub

    Private Sub dlMenu_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlMenu.ItemDataBound

        If e.Item.DataItem("URL") = "" Then
            e.Item.FindControl("label").Visible = True
        Else
            e.Item.FindControl("link").Visible = True
        End If

    End Sub
End Class
