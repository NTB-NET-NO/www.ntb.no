<%@ Page Language="vb" AutoEventWireup="false" Codebehind="orderform.aspx.vb" Inherits="ntb.orderform"%>
<%@ Register TagPrefix="custom" TagName="leftmenu" src="leftmenu.ascx"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>orderform</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<table>
			<tr>
				<td valign="top">
					<FORM id="frmEmail" method="post" runat="server">
						<INPUT type=hidden value='Tilgang til <%Response.Write(Request.QueryString("Section"))%>' name="__SUBJECT">
						<INPUT type="hidden" value="jhb@hjem.no" name="__FROM"> <INPUT type=hidden value='Tilgang til <%Response.Write(Request.QueryString("Section"))%>' name="__TITLE">
						<INPUT type=hidden value='<%Response.Write(Request.QueryString("To"))%>' name="__TO">
						<table class="artikkel_tekst" cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr>
								<td vAlign="top" align="left" colSpan="2">
									<br>
									<b>Bestille tilgang.</b><br>
									Her kan du bestille tilgang til NTB tjenester.<br>
									Fyll ut skjemaet under og du vil bli tildelt et brukernavn og passord.<br>
									Tilgangen til enkeltsaker er gratis, men all bruk(nedlasting) vil bli 
									fakturert.
									<br>
									<br>
									<br>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="right"><b>Tilgangsform:</b></td>
								<td vAlign="top"><input onclick="this.form.Enkeltsaker.checked=false;" type="checkbox" CHECKED value="Abonnement"
										name="Abonnement"> <b>Abonnement</b></td>
							</tr>
							<tr>
								<td style="HEIGHT: 19px" align="right"><b>Produkt:</b></td>
								<td style="HEIGHT: 19px"><asp:DropDownList id="Produktet" runat="server"></asp:DropDownList></td>
							</tr>
							<tr>
								<td align="right"><b>Medium/organisasjon:</b></FONT></td>
								<td><asp:textbox id="Medium" runat="server" Width="224px"></asp:textbox>&nbsp;*
									<asp:requiredfieldvalidator id="Req1" runat="server" ErrorMessage="Feltet mangler verdi!" ControlToValidate="Medium"
										Font-Names="Verdana" Font-Size="11px"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td align="right"><b>Kontaktperson:</b>
								</td>
								<td><asp:textbox id="Kontakt" runat="server" Width="224px"></asp:textbox>&nbsp;*
									<asp:requiredfieldvalidator id="Req2" runat="server" ErrorMessage="Feltet mangler verdi!" ControlToValidate="Kontakt"
										Font-Names="Verdana" Font-Size="11px"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td style="HEIGHT: 27px" align="right"><b>Post adresse:</b></td>
								<td style="HEIGHT: 27px"><asp:textbox id="Adresse" runat="server" Width="224px"></asp:textbox>&nbsp;*
									<asp:requiredfieldvalidator id="Req3" runat="server" ErrorMessage="Feltet mangler verdi!" ControlToValidate="Adresse"
										Font-Names="Verdana" Font-Size="11px"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td align="right"><b>Postnr. og sted:</b></td>
								<td><asp:textbox id="PostNr" runat="server"></asp:textbox>&nbsp;*
									<asp:requiredfieldvalidator id="Req4" runat="server" ErrorMessage="Feltet mangler verdi!" ControlToValidate="PostNr"
										Font-Names="Verdana" Font-Size="11px"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td align="right"><b>Telefon Arbeid:</b></td>
								<td><asp:textbox id="Tlf" runat="server" Width="88px"></asp:textbox>&nbsp;*&nbsp;
									<asp:requiredfieldvalidator id="Req5" runat="server" ErrorMessage="Feltet mangler verdi!" ControlToValidate="Tlf"
										Font-Names="Verdana" Font-Size="11px"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td align="right"><b>Telefax:</b></td>
								<td><asp:textbox id="Fax" runat="server" Width="88px"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right"><b>E-post:</b></td>
								<td><asp:textbox id="Email" runat="server" Width="176px"></asp:textbox>&nbsp;*
									<asp:requiredfieldvalidator id="Req6" runat="server" ErrorMessage="Feltet mangler verdi!" ControlToValidate="Email"
										Font-Names="Verdana" Font-Size="11px"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td align="right"><b>Målgruppe:</b></td>
								<td><asp:textbox id="Malgruppe" runat="server" Width="176px"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right"><b>Dekningsområde</b></td>
								<td><asp:textbox id="Dekning" runat="server" Width="200px"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right"><b>Opplag:</b></td>
								<td><asp:textbox id="Opplag" runat="server" Width="88px"></asp:textbox></td>
							</tr>
							<tr>
								<td vAlign="top" align="right"><b>Kommentar:</b></td>
								<td><asp:textbox id="Kommentar" runat="server" Width="232px" Height="64px"></asp:textbox></td>
							</tr>
							<tr>
								<td></td>
								<td vAlign="top"><asp:button class="formbutton" id="btnSubmit" runat="server" Text="Send bestilling"></asp:button><br>
									<br>
									Felt merket med * er påkrevet
								</td>
							</tr>
						</table>
					</FORM>
				</td>
			</tr>
		</table>
	</body>
</HTML>
