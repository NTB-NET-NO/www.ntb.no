Imports System.Data.SqlClient

Partial Class printerfriendly_list
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected dr As SqlDataReader
    Protected titleMap As New Hashtable
    Protected sectionMap As New Hashtable

    Protected olddate, dateheader As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        titleMap.Add("NYHETERMEDIER", "NTB Nyheter for medieredaksjoner")
        titleMap.Add("NYHETERBEDRIFTER", "NTB Nyheter for bedrifter")
        titleMap.Add("KULTUR", "Kultur og underholdning")
        titleMap.Add("SHOWBIZ", "Showbiz og glamour")
        titleMap.Add("FEATURE", "NTB Feature")
        titleMap.Add("TIPPETIPS", "Tippetips og spill")
        titleMap.Add("PERSONALIA", "Personalia, omtale- og jubilant-tjenester")
        titleMap.Add("DID", "Dagen i dag")
        titleMap.Add("FOTO", "Foto til NTB-nyhetene")
        titleMap.Add("BORS", "Ferdigsider b�rs")
        titleMap.Add("LOKALTJENESTEN", "NTBs lokaltjeneste - ditt Oslokontor")
        titleMap.Add("LYD", "NTB Lyd")
        titleMap.Add("TEKSTTV", "NTB p� tekst-tv")
        titleMap.Add("TICKER", "Nyhetsticker fra NTB")
        titleMap.Add("MOBIL", "NTB Mobil")
        titleMap.Add("SMSVARSLING", "NTB Varsling")
        titleMap.Add("NYHETSKALENDER", "Nyhetskalender")
        titleMap.Add("NYHETERPAANETT", "NTB Direkte p� internett")
        titleMap.Add("SELEKTERT", "NTB Selektert - utvalgte nyheter")
        titleMap.Add("FAXAVIS", "FaxAvisen")
        titleMap.Add("PRESSEMELDINGER", "Pressemeldinger")
        titleMap.Add("DISTRIBUSJON", "Distribusjon")
        titleMap.Add("MEDIEOVERV�KING", "Medieoverv�king")
        titleMap.Add("NYHETSARKIV", "NTB Arkiv")
        titleMap.Add("WAP", "NTB p� Wap")

        Dim t1 As String()
        t1 = New String() { _
        "NYHETERMEDIER", _
        "NYHETERPAANETT", _
        "KULTUR", _
        "SHOWBIZ", _
        "FEATURE", _
        "TIPPETIPS", _
        "PERSONALIA", _
        "DID", _
        "FOTO", _
        "BORS", _
        "LOKALTJENESTEN", _
        "LYD", _
        "TEKSTTV", _
        "TICKER", _
        "MOBIL", _
        "SMSVARSLING", _
        "NYHETSKALENDER"}
        sectionMap.Add("MEDIER_LIST", t1)

        t1 = New String() { _
        "NYHETERBEDRIFTER", _
        "NYHETERPAANETT", _
        "SELEKTERT", _
        "FAXAVIS", _
        "PRESSEMELDINGER", _
        "DISTRIBUSJON", _
        "MEDIEOVERV�KING", _
        "NYHETSKALENDER", _
        "NYHETSARKIV", _
        "SMSVARSLING", _
        "MOBIL", _
        "WAP"}
        sectionMap.Add("BEDRIFTER_LIST", t1)

        If (sectionMap.ContainsKey(Request.QueryString("section") Or _
            titleMap.ContainsKey(Request.QueryString("section")))) Then
            BindItems(dlItems)
        End If

    End Sub

    Private Sub BindItems(ByVal list As DataList)
        Dim db As New SqlConnection(ConfigurationManager.AppSettings("ConString"))
        Dim cd As New SqlCommand

        cd.Connection = db
        cd.CommandText = "_GetSections"
        cd.CommandType = CommandType.StoredProcedure

        Dim ss As String()
        If sectionMap.ContainsKey(Request.QueryString("section")) Then
            ss = sectionMap(Request.QueryString("section"))
        Else
            ss = Request.QueryString.GetValues("section")
        End If

        Dim tmp As String = String.Empty
        Dim s As String

        For Each s In ss
            tmp = tmp & s & "','"
        Next
        tmp = tmp.Substring(0, tmp.Length() - 3)

        cd.Parameters.Add(New SqlParameter("@sections", SqlDbType.VarChar, 1000))
        cd.Parameters.Item("@sections").Value = "('" & tmp & "')"
        cd.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
        cd.Parameters.Item("@sitecode").Value = Session("SiteCode")

        db.Open()
        dr = cd.ExecuteReader()

        list.DataSource = dr
        list.DataBind()

        dr.Close()
        db.Close()
    End Sub

    Private Sub dlItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlItems.ItemDataBound
        'Dim name As String = ""
        'Dim ctrl As System.Web.UI.WebControls.Label = e.Item.FindControl("name")

        'If Not dr.IsDBNull(dr.GetOrdinal("real_name")) Then
        '    name = dr("real_name")
        'End If

        'If Not dr.IsDBNull(dr.GetOrdinal("name")) And name = "" Then
        '    name = dr("name")
        'End If

        'If Not dr.IsDBNull(dr.GetOrdinal("username")) And name = "" Then
        '    name = dr("username")
        'End If

        'ctrl.Text = name

    End Sub

    Private Sub dlItems_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlItems.ItemCreated

        'If olddate <> Format(CType(sender, DataList).DataSource("start_date"), "yyyyMM") Then
        '    dateheader = "<p class=""artikkel_tekst""><b>" & Format(CType(sender, DataList).DataSource("start_date"), "MMMM, yyyy").ToUpper & "</b></p>"
        'Else
        '    dateheader = ""
        'End If

        'olddate = Format(CType(sender, DataList).DataSource("start_date"), "yyyyMM")

    End Sub
End Class
