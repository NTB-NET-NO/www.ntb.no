<%@ Register TagPrefix="custom" TagName="leftmenu" src="leftmenu.ascx"%>
<%@ Page EnableSessionState="true" Language="vb" AutoEventWireup="false" Codebehind="login.aspx.vb" Inherits="ntb.login"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>login</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="css/styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body runat="server" id="body">
		<table cellSpacing="0" cellPadding="0" width="436" border="0">
			<tr>
				<td vAlign="top">
					<br>
					<div class="artikkel_tekst">For � lese denne tjenesten m� du v�re abonnent hos NTB.<br>
						For informasjon om priser og betingelser kontakt markedsavdelingen.<br>
						E-post: <a href="mailto:marked@ntb.no">marked@ntb.no</a><br>
						Telefon: 22 03 45 09<br>
					</div>
					<br>
					<br>
					<div align="center"><form id="frmLogin" method="post" runat="server">
							<table>
								<tr>
									<P><td valign="top"><asp:label id="Label2" runat="server" CssClass="artikkel_liste_dato_tid">Brukernavn: </asp:label></td>
										<td>
									<P><asp:textbox Width="150" id="txtUsername" runat="server"></asp:textbox>&nbsp;<asp:requiredfieldvalidator id="ReqUsername" runat="server" ErrorMessage="Skriv inn brukernavn!" ControlToValidate="txtUsername"
											CssClass="artikkel_liste_dato_tid"></asp:requiredfieldvalidator></P>
				</td>
				</P></tr>
			<tr>
				<P><td valign="top"><asp:label id="Label1" runat="server" CssClass="artikkel_liste_dato_tid">Passord: </asp:label></td>
					<td>
				<P><asp:textbox width="150" id="txtPassword" runat="server" TextMode="Password"></asp:textbox>&nbsp;<asp:requiredfieldvalidator id="ReqPassword" runat="server" ErrorMessage="Skriv inn passord!" ControlToValidate="txtPassword"
						CssClass="artikkel_liste_dato_tid"></asp:requiredfieldvalidator></P>
				</td></P></tr>
			<tr>
				<P><td></td>
					<td><asp:button class="formbutton" id="btnLogin" runat="server" Text="Login"></asp:button>
				<P></P>
				</td></P></tr>
			<tr>
				<td colspan="2" class="artikkel_tekst">Glemt passordet? Klikk <a href="forgotpassword.aspx">
						her</a> for hjelp.</td>
			</tr>
		</table>
		</form> </div>
		<script language="javascript">
	  if (frmLogin.txtUsername.value == "") {frmLogin.txtUsername.focus()} else {frmLogin.txtPassword.focus()};
		</script>
		</td> </tr></table>
	</body>
</HTML>
