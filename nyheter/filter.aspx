<%@ Page EnableSessionState="true" Language="vb" AutoEventWireup="false" Codebehind="filter.aspx.vb" Inherits="ntb.filter"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>filter</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		
		<script Language="Javascript">

			function off(c) {
				if (c.checked) {document.searchform.alle.checked = false; }
			}

			function alloff(c) {
				if (c.checked) {
				for (j=1;j<=17;j++) {
				eval("document.searchform.k"+j).checked = false;
				}
				}
			}

			function DoFilter(f) {
				var ts = "";
				var maincat = 0;
				ts = "Interval=" + document.searchform.Interval.options[document.searchform.Interval.selectedIndex].value;
				ts = ts + "&MainGroup=" + document.searchform.Stoffgruppe.options[document.searchform.Stoffgruppe.selectedIndex].value;
				if (document.searchform.alle.checked == false) {
				for (j=1;j<=17;j++) {
					if (eval("document.searchform.k"+j).checked) {
						maincat += parseInt(eval("document.searchform.k"+j).value)
					}
					} 

					if (maincat == 0) { maincat = parseInt(document.searchform.alle.value); }
				} 
				else { 
					if (document.searchform.alle.value != "-1") { maincat = parseInt(document.searchform.alle.value); } 
				}

				if (maincat > 0) { ts = ts + "&MainCat=" + maincat };
				parent.articlelist.document.location = "../articles.aspx?section=IDAG&" + ts;
			}

			function DoFilter2(f) {
				var ts = "";
				var t = "";
				ts = "Interval=" + document.searchform.Interval.options[document.searchform.Interval.selectedIndex].value;
				ts = ts + "&MainGroup=" + document.searchform.Stoffgruppe.options[document.searchform.Stoffgruppe.selectedIndex].value;
				if (document.searchform.alle.checked == false) {
				for (j=1;j<=17;j++) {
				    if (eval("document.searchform.k"+j).checked) {
						if (t == "") {
							t = t + eval("document.searchform.k"+j).value }
						else
						{
							t = t + "," + eval("document.searchform.k"+j).value } 
					}
				} if (t == "") { t = document.searchform.alle.value; }
				} else { 
					if (document.searchform.alle.value != "-1") { 
					  t = document.searchform.alle.value; } 
				  }

				if (t != "") { ts = ts + "&MainCat="+t;}
				parent.articlelist.document.location = "../articles.aspx?section=IDAG&" + ts;
			}
		</script>
	
	</HEAD>
	<body>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
			<tr>
				<td valign="top">
					<img src="images/t.gif" height=4 width=1><br>
		
					<form name="searchform" action="" method="post" style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px">
					<input type="hidden" name="cookiename" value="NTBIDAGSOK">

					<table width="645" cellspacing="0" cellpadding="2" border="0" bgcolor="#eeeeee" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid">
						<tr>
							<td valign="top">
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><font class="news11">Stoffgruppe : </font>
												<br>
												<select class="news11" name="Stoffgruppe">
													<% Response.Write(maingroups) %>
												</select>
											</td>
										</tr>
										<tr>
											<td><img height="6"></td>
										</tr>
										<tr>
											<td><font class="news11">Intervall : </font>
												<br>
												<SELECT class="news11" NAME="Interval">
													<option value="24" selected>Siste 24 timer</option> 
													<option value="168">Siste uke</option>
													<% Response.Write(dayIntervals) %>
												</SELECT><br>
											</td>
										</tr>
										<tr>
											<td><img height="6"></td>
										</tr>
										<tr>
											<td><input class="formbutton" type="button" value="Hent" onClick="DoFilter();">&nbsp;<input type="button" class="formbutton" value="Lagre" onclick="pbsSetCookie(document.searchform); alert('Utvalget du har gjort er lagret og vil automatisk hentes opp neste gang du er p� denne siden.');"
													id="Button1" name="Button1">
											</td>
										</tr>
									</table>
							</td>
							<td valign="top">
								<table cellpadding="0" cellspacing="0" border="0">
									
									<!--tr>
										<td valign="top">
											<font class="news11">
												<input onclick="alloff(this);" type="checkbox" value="<%= Access(-1)%>" name="alle" checked> <b>Alle</b><br>
												<input onclick="off(this);" type="checkbox" value="1" name="k1" <%= Access(1)%>> Arbeidsliv<br>
												<input onclick="off(this);" type="checkbox" value="2" name="k2" <%= Access(2)%>> Fritid<br>
												<input onclick="off(this);" type="checkbox" value="4" name="k3" <%= Access(4)%>> Krig/konflikter<br>
												<input onclick="off(this);" type="checkbox" value="8" name="k4" <%= Access(8)%>> Kriminalitet/rettsvesen<br>
												<input onclick="off(this);" type="checkbox" value="16" name="k5" <%= Access(16)%>> Kultur/underholdning<br>
												<input onclick="off(this);" type="checkbox" value="32" name="k6" <%= Access(32)%>> Kuriosa/kjendiser<br>
												<input onclick="off(this);" type="checkbox" value="64" name="k7" <%= Access(64)%>> Medisin/helse<br>
												<input onclick="off(this);" type="checkbox" value="128" name="k8" <%= Access(128)%>> Natur/milj�<br>
											</font>
										</td>
										<td valign="top">
											<font class="news11">
												<input onclick="off(this);" type="checkbox" value="256" name="k9" <%= Access(256)%>> Politikk<br>
												<input onclick="off(this);" type="checkbox" value="512" name="k10" <%= Access(512)%>> Religion/livssyn<br>
												<input onclick="off(this);" type="checkbox" value="1024" name="k11" <%= Access(1024)%>> Sosiale forhold<br>
												<input onclick="off(this);" type="checkbox" value="2048" name="k12" <%= Access(2048)%>>	Sport<br>
												<input onclick="off(this);" type="checkbox" value="4096" name="k13" <%= Access(4096)%>> Ulykker/naturkatastrofer<br>
												<input onclick="off(this);" type="checkbox" value="8192" name="k14" <%= Access(8192)%>> Utdanning<br>
												<input onclick="off(this);" type="checkbox" value="16384" name="k15" <%= Access(16384)%>> Vitenskap/teknologi<br>
												<input onclick="off(this);" type="checkbox" value="32768" name="k16" <%= Access(32768)%>> V�r<br>
												<input onclick="off(this);" type="checkbox" value="65536" name="k17" <%= Access(65536)%>> �konomi/n�ringsliv
											</font>
										</td>
									</tr-->

									
									<tr>
										<td valign="top">
											<font class="news11">
												<input onclick="alloff(this);" type="checkbox" value="<%= Access(-1)%>" name="alle" checked> <b>Alle</b><br>
												<input onclick="off(this);" type="checkbox" value="1" name="k1" <%= Access(1)%>> Arbeidsliv<br>
												<input onclick="off(this);" type="checkbox" value="2" name="k2" <%= Access(2)%>> Fritid<br>
												<input onclick="off(this);" type="checkbox" value="4" name="k3" <%= Access(4)%>> Krig/konflikter<br>
												<input onclick="off(this);" type="checkbox" value="8" name="k4" <%= Access(8)%>> Kriminalitet/rettsvesen
											</font>
										</td>
										<td valign="top">
											<font class="news11">
												<input onclick="off(this);" type="checkbox" value="16" name="k5" <%= Access(16)%>> Kultur/underholdning<br>
												<input onclick="off(this);" type="checkbox" value="32" name="k6" <%= Access(32)%>> Kuriosa/kjendiser<br>
												<input onclick="off(this);" type="checkbox" value="64" name="k7" <%= Access(64)%>> Medisin/helse<br>
												<input onclick="off(this);" type="checkbox" value="128" name="k8" <%= Access(128)%>> Natur/milj�<br>
												<input onclick="off(this);" type="checkbox" value="256" name="k9" <%= Access(256)%>> Politikk
											</font>
										</td>
										<td valign="top">
											<font class="news11">
												<input onclick="off(this);" type="checkbox" value="512" name="k10" <%= Access(512)%>> Religion/livssyn<br>
												<input onclick="off(this);" type="checkbox" value="1024" name="k11" <%= Access(1024)%>> Sosiale forhold<br> 
												<input onclick="off(this);" type="checkbox" value="2048" name="k12" <%= Access(2048)%>> Sport<br>
												<input onclick="off(this);" type="checkbox" value="4096" name="k13" <%= Access(4096)%>> Ulykker/naturkatastrofer<br>
												<input onclick="off(this);" type="checkbox" value="8192" name="k14" <%= Access(8192)%>> Utdanning
											</font>
										</td>
										<td valign="top">
											<font class="news11">
												<input onclick="off(this);" type="checkbox" value="16384" name="k15" <%= Access(16384)%>> Vitenskap/teknologi<br>
												<input onclick="off(this);" type="checkbox" value="32768" name="k16" <%= Access(32768)%>> V�r<br>
												<input onclick="off(this);" type="checkbox" value="65536" name="k17" <%= Access(65536)%>> �konomi/n�ringsliv
											</font>
										</td>
									</tr>
								</table>
								</FORM>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<script language="javascript" src="../includes/cookiesearchparams.js"></script>
		<script language="Javascript">DoFilter(document.searchform);</script>
	</body>
</HTML>
