Imports System.Data.SqlClient
Imports ntb.[Global]

Partial Class filter
    Inherits System.Web.UI.Page
    Protected days As String() = {"S�ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "L�rdag"}
    Protected maingroups As String
    Protected dayIntervals As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub DoIplogin()
        Dim iploggedinuser As String = ""
        iploggedinuser = CheckIP(Request.UserHostAddress, Session("CurrentArticleMainGroup"))
        If iploggedinuser <> "" Then
            Session("UserId") = iploggedinuser

            AddUserHit(Session("UserID"), True, False, Session("SiteCode"))

            Dim arr As Array = Split(GetUserData(Session("UserId"), "MainGroups"), ",")
            Dim i As Integer
            For i = 0 To arr.Length - 1
                If arr(i) <> "" Then Session("MainGroup" & CStr(arr(i))) = "1"
            Next
        End If
    End Sub

    Public Function Access(ByVal maincat As Integer) As String
        Access = " disabled"
        If Session("UserId") = "" Then Exit Function
        Access = ""
        If Session("MainCats") = "" Then Session("MainCats") = GetUserData(Session("UserId"), "MainCats")
        If maincat = -1 Then
            Dim arr As Array = Split(Session("MainCats"), ",")
            Dim i, sum As Integer
            For i = 0 To arr.Length - 1
                If arr(i) <> "" Then sum += CInt(arr(i))
            Next
            Access = CStr(sum)
        Else
            Dim arr As Array = Split(Session("MainCats"), ",")
            If arr(0) = "-1" Then
                Access = ""
            Else
                If Array.IndexOf(arr, CStr(maincat)) = -1 Then Access = " disabled"
            End If
        End If
    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Session("UserId") = "" Then
            Session("CurrentArticleMainGroup") = "-1"
            DoIplogin()
        Else
            AddUserHit(Session("UserID"), False, False, Session("SiteCode"))
        End If

        maingroups = GetMainGroupOptions(Session("UserId"))

        'Create day intervals
        Dim span As TimeSpan = New TimeSpan(1, 0, 0, 0, 0)
        Dim d1 As Date = Today
        Dim d2 As Date = d1.Subtract(span)
        Dim n As DateTime = Now

        Dim i As Integer
        For i = 0 To 6
            dayIntervals &= "<option value=""" & Math.Ceiling(n.Subtract(d2).TotalHours) & "-" & Math.Floor(n.Subtract(d1).TotalHours) & """>" & days(d2.DayOfWeek()) & " " & d2.ToString("dd.MM") & "</option>" & vbCrLf
            d1 = d1.Subtract(span)
            d2 = d2.Subtract(span)
        Next

    End Sub

End Class
